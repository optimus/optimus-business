<?php
include_once 'libs/datatables.php';

$get = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('owner', $input->path[3], 'strictly_positive_integer', true);

	$odt = rest('https://api.' . getenv('DOMAIN') . '/optimus-business/' . $input->owner . '/invoices/' . $input->id . '/odt', 'GET', '{}', $_COOKIE['token']);
	if ($odt->code != 200)
		return array("code" => $odt->code, "message" => $odt->message);

	exec ('rm /srv/files/tmp/invoice_' . $input->owner . '_' . $input->id . '.odt');
	file_put_contents('/srv/files/tmp/invoice_' . $input->owner . '_' . $input->id . '.odt', base64_decode($odt->data));
	
	exec ('rm /srv/files/tmp/invoice_' . $input->owner . '_' . $input->id . '.pdf');
	$pdf = rest('https://api.' . getenv('DOMAIN') . '/optimus-drive/convert-pdf', 'GET', '{"file":"files/tmp/invoice_' . $input->owner . '_' . $input->id . '.odt"}', $_COOKIE['token']);
	if ($pdf->code != 200)
		return array("code" => $pdf->code, "message" => $pdf->message);

	if ($_GET['mode'] == 'download')
	{
		$invoice = rest('https://api.' . getenv('DOMAIN') . '/optimus-business/' . $input->owner . '/invoices/' . $input->id, 'GET', '{}', $_COOKIE['token']);
		if ($invoice->code != 200)
			return array("code" => $invoice->code, "message" => $invoice->message);
		header('Content-type:octet-stream');
		header('Content-Disposition:attachment;filename="' . $invoice->data->number . '.pdf"');
		readfile('/srv/files/tmp/invoice_' . $input->owner . '_' . $input->id . '.pdf');
		exec ('rm /srv/files/tmp/invoice_' . $input->owner . '_' . $input->id . '.odt');
		exec ('rm /srv/files/tmp/invoice_' . $input->owner . '_' . $input->id . '.pdf');
	}
	if ($_GET['mode'] == 'view')
	{
		$invoice = rest('https://api.' . getenv('DOMAIN') . '/optimus-business/' . $input->owner . '/invoices/' . $input->id, 'GET', '{}', $_COOKIE['token']);
		if ($invoice->code != 200)
			return array("code" => $invoice->code, "message" => $invoice->message);
		header('Content-type:application/pdf');
		readfile('/srv/files/tmp/invoice_' . $input->owner . '_' . $input->id . '.pdf');
		exec ('rm /srv/files/tmp/invoice_' . $input->owner . '_' . $input->id . '.odt');
		exec ('rm /srv/files/tmp/invoice_' . $input->owner . '_' . $input->id . '.pdf');
	}
	else
	{
		$file_content = base64_encode(file_get_contents('/srv/files/tmp/invoice_' .$input->owner . '_' . $input->id . '.pdf'));
		exec ('rm /srv/files/tmp/invoice_' . $input->owner . '_' . $input->id . '.odt');
		exec ('rm /srv/files/tmp/invoice_' . $input->owner . '_' . $input->id . '.pdf');
		return array("code" => 200, "data" => $file_content, "authorizations" => $authorizations);

	}
	
};
?>