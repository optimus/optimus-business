<?php
include_once 'libs/datatables.php';
$resource = json_decode('
{
	"id": { "type": "positive_integer", "field": "partners.id", "post": ["ignored"], "patch": ["immutable"], "default": 0 },
	"server": { "type": "domain", "field": "partners.server", "post": ["required", "notnull", "notempty"], "patch": ["notnull", "notempty"], "default": "' . getenv('DOMAIN') . '" },
	"user": { "type": "strictly_positive_integer", "field": "partners.user", "post": ["required", "notnull", "notempty"], "patch": ["notnull", "notempty"], "default": 0 },
	"arrival": { "type": "date", "field": "partners.arrival", "post": ["required", "notnull", "notempty"], "patch": ["notnull", "notempty"], "default": "" },
	"departure": { "type": "date", "field": "partners.departure", "post": ["notempty"], "patch": ["emptytonull"], "default": "" }
}
', null, 512, JSON_THROW_ON_ERROR);

$get = function ()
{
	global $connection, $input, $resource;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);

	$partners = $connection->query("SELECT * FROM `user_" . $input->owner . "`.`partners` ORDER BY arrival")->fetchAll(PDO::FETCH_ASSOC);

	for($i=0; $i < sizeof($partners); $i++)
		if ($partners[$i]['server'] == getenv('DOMAIN'))
		{
			$partner_info = $connection->query("SELECT displayname FROM server.users WHERE id = '" . $partners[$i]['user'] . "'")->fetch(PDO::FETCH_ASSOC);
			$partners[$i]['displayname'] = $partner_info['displayname'];
		}
		else
		{
			$partner_info = rest('https://api.' . $partners[$i]['server'] . '/optimus-base/users/' . $partners[$i]['user'], 'GET', null, $_COOKIE['token']);
			if ($partner_info->code == 200)
				$partners[$i]['displayname'] = $response->data->displayname;
			else
				return array("code" => 400, "message" => "Les informations de l'utilisateur " . $partners[$i]['user'] . " n'ont pas pu être récupérées sur le serveur " . $partners[$i]['server']);
		}
	return array("code" => 200, "data" => sanitize($resource, $partners));
};


$post = function ()
{
	global $connection, $input, $resource;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);

	check_input_body($resource, 'post');

	if (!is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - Seul un administrateur peut ajouter un associé");

	$user_info = rest('https://api.' . $input->body->server . '/optimus-base/users/' . $input->body->user, 'GET', null, $_COOKIE['token']);
	if ($user_info->code != 200)
		return array("code" => 400, "message" => "Les informations de l'utilisateur " .  $input->body->user . " n'ont pas pu être récupérées sur le serveur " . $input->body->server);

	$partner_already_in_business = $connection->prepare('SELECT id FROM `user_' . $input->owner . '`.`partners` WHERE server = :server AND user = :user');
	$partner_already_in_business->bindParam(':server', $input->body->server, PDO::PARAM_STR);
	$partner_already_in_business->bindParam(':user', $input->body->user, PDO::PARAM_STR);
	$partner_already_in_business->execute();
	if ($partner_already_in_business->rowCount() > 0)
		return array("code" => 400, "message" => "Cet associé est déjà présent");
	
	if (isset($input->body->departure) AND strtotime($input->body->arrival) > strtotime($input->body->departure))
		return array("code" => 400, "message" => "La date d'entrée doit se situer avant la date de sortie");
	
	$connection->query("INSERT IGNORE INTO `user_" . $input->body->user . "`.`businesses` SET server = '" . $input->body->server . "', business_id = '" . $input->owner . "'");
	
	$authorization = (object)array("user"=> $input->body->user, "owner" => $input->owner, "resource" => "invoices", "read" => 1, "write" => 1, "create" => 1, "delete" => 1);
	rest('https://api.' . getenv('DOMAIN') . '/optimus-base/authorizations', 'POST', json_encode($authorization), $_COOKIE['token']);
	$authorization = (object)array("user"=> $input->body->user, "owner" => $input->owner, "resource" => "files", "read" => 1, "write" => 1, "create" => 1, "delete" => 1);
	rest('https://api.' . getenv('DOMAIN') . '/optimus-base/authorizations', 'POST', json_encode($authorization), $_COOKIE['token']);

	$query = datatables_insert($connection, $resource, 'user_' . $input->owner, 'partners');
	if($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $connection->lastInsertId() . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'partners');
		$results[0]['displayname'] = $user_info->data->displayname;
		return array("code" => 201, "data" => sanitize($resource, $results[0]), "message" => "Associé créé avec succès");
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);
};


$patch = function ()
{
	global $connection, $input, $resource;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);

	check_input_body($resource, 'patch');

	if (!is_admin($input->user->id))
		return array("code" => 401, "message" => "Accès refusé - Seul un administrateur peut modifier un associé");
	
	$exists = $connection->query("SELECT * FROM `user_" . $input->owner . "`.`partners` WHERE id = '" . $input->id . "'")->fetch(PDO::FETCH_OBJ);
	if (!$exists)
		return array("code" => 409, "message" => "Cet associé n'existe pas");

	$arrival = $input->body->arrival ?: $exists->arrival;
	$departure = $input->body->departure ?: $exists->departure;
	if ($departure AND strtotime($arrival) > strtotime($departure))
		return array("code" => 400, "message" => "La date d'entrée doit se situer avant la date de sortie");
	
	$query = datatables_update($connection, $resource, 'user_' . $input->owner, 'partners', $input->id);
	if($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $input->id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'partners');
		return array("code" => 200, "data" => sanitize($resource, $results[0]), "message" => "Utilisateur modifié avec succès");
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);
};


$delete = function ()
{
	global $connection, $input, $domain;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);

	if (!is_admin($input->user->id))
		return array("code" => 403, "message" => "Accès refusé - Seul un administrateur peut supprimer un associé");

	$exists = $connection->query("SELECT * FROM `user_" . $input->owner . "`.`partners` WHERE id = " . $input->id)->fetch(PDO::FETCH_OBJ);
	if (!$exists)
		return array("code" => 409, "message" => "Cet associé n'existe pas");
	
	$factures_exists = $connection->query("SELECT id FROM `user_" . $input->owner . "`.`invoices` WHERE details_server='" . getenv('DOMAIN') . "' AND details_owner = '" . $exists->user . "'")->fetch(PDO::FETCH_ASSOC);
	if ($factures_exists)
		return array("code" => 409, "message" => "Cet associé ne peut pas être supprimé car il a créé des factures dans cette entreprise");

	$authorizations = rest('https://api.' . getenv('DOMAIN') . '/optimus-base/authorizations', 'GET', '{"owner":' . $input->owner . ', "user":' . $exists->user . '}', $_COOKIE['token'])->data;
	foreach($authorizations as $authorization)
		rest('https://api.' . getenv('DOMAIN') . '/optimus-base/authorizations/' . $authorization->id, 'DELETE', '{}', $_COOKIE['token']);

	$connection->query("DELETE FROM `user_" . $exists->user . "`.`businesses` WHERE server ='" . getenv('DOMAIN') . "' AND business_id = '" . $input->owner . "'");
	
	$delete = $connection->query("DELETE FROM `user_" . $input->owner . "`.`partners` WHERE id = '" . $input->id . "'");
	return array("code" => 200);
};
?>