<?php
include('libs/datatables.php');

$get = function ()
{
	global $connection, $resource, $input;
	
	auth();
	allowed_origins_only();
		
	$input->constant = check('id', $input->path[2], 'module', true);

	$resource = new stdClass();
	if (in_array($input->constant, array("accounting_methods", "invoices_numbering_formats", "invoices_reminders_types", "vat_schemes")))
	{
		$resource->id = (object) array("type" => "strictly_positive_integer", "field" => $input->constant . ".id");
		$resource->value = (object) array("type" => "string", "field" => $input->constant . ".value");
	}
	else if (in_array($input->constant, array("vat_rates")))
	{
		$resource->id = (object) array("type" => "strictly_positive_integer", "field" => $input->constant . ".id");
		$resource->value = (object) array("type" => "string", "field" => $input->constant . ".value");
		$resource->rate = (object) array("type" => "decimal", "field" => $input->constant . ".rate");
	}
	else
		return array("code" => 404, "message" => "La constante demandée n'existe pas");

	$results = datatable_request($connection, (object) $resource, 'common', $input->constant);
	$last_row = (int)$connection->query('SELECT FOUND_ROWS()')->fetchColumn();
	$last_page = $input->body->size > 0 ? ceil(max($last_row,1) / $input->body->size) : 1;
	
	return array("code" => 200, "data" => $results, "last_row" => $last_row, "last_page" => $last_page);
};
?>