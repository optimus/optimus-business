<?php
include_once 'libs/websocket_client.php';
use optimus\OptimusWebsocket;

$get = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->start_date = check('start_date', $input->body->start_date, 'date', true);
	$input->end_date = check('start_date', $input->body->end_date, 'date', true);
	
	$invoices = $connection->prepare("SELECT id, number FROM `user_" . $input->owner . "`.invoices WHERE date BETWEEN :start_date AND :end_date ORDER BY number");
	$invoices->bindParam(':start_date', $input->body->start_date);
	$invoices->bindParam(':end_date', $input->body->end_date);
	$invoices->execute();
	
	if ($invoices->rowCount() == 0)
		return array("code" => 404, "message" => "Aucune facture trouvée sur cette période");
	
	$invoices = $invoices->fetchAll(PDO::FETCH_OBJ);
	
	$zip = new ZipArchive();
	$zip->open('/srv/files/tmp/export_invoices.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);

	$websocket = new OptimusWebsocket($input->user->id, $input->user->email, is_admin($input->user->id));
	$websocket->send(json_encode(array("command" => "optimus-business_export_invoices", "user" => $input->user->id, "data" => array("value" => 0, "max" => sizeof($invoices)))));

	foreach ($invoices as $invoice)
	{
		if (exists($connection, 'user_' . $input->owner, 'partners', 'user', $input->user->id) OR is_admin($input->user->id))
			$restrictions = [];
		else
		{
			$restrictions = get_restrictions($input->user->id, $input->owner, 'invoices/' . $input->id);
			if (in_array('read', $restrictions))
				return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour générer cette facture");
		}

		exec ('rm /srv/files/tmp/invoice_' . $input->owner . '_' . $invoice->id . '.odt');
		$odt = rest('https://api.' . getenv('DOMAIN') . '/optimus-business/' . $input->owner . '/invoices/' . $invoice->id . '/odt', 'GET', '{}', $_COOKIE['token']);
		if ($odt->code != 200)
			return array("code" => $odt->code, "message" => $odt->message);
		file_put_contents('/srv/files/tmp/invoice_' . $input->owner . '_' . $invoice->id . '.odt', base64_decode($odt->data));

		exec ('rm /srv/files/tmp/invoice_' . $input->owner . '_' . $invoice->id . '.pdf');
		$pdf = rest('https://api.' . getenv('DOMAIN') . '/optimus-drive/convert-pdf', 'GET', '{"file":"files/tmp/invoice_' . $input->owner . '_' . $invoice->id . '.odt"}', $_COOKIE['token']);
		if ($pdf->code != 200)
			return array("code" => $pdf->code, "message" => $pdf->message);

		$zip->addFile('/srv/files/tmp/invoice_' . $input->owner . '_' . $invoice->id . '.pdf', $invoice->number . '.pdf');

		$x++;
		$websocket->send(json_encode(array("command" => "optimus-business_export_invoices", "user" => $input->user->id, "data" => array("value" => $x, "max" => sizeof($invoices)))));
	}

	if (!$zip->close())
		return array("code" => 400, "message" => "Une erreur est survenue");

	foreach ($invoices as $invoice)
	{
		exec ('rm /srv/files/tmp/invoice_' . $input->owner . '_' . $invoice->id . '.odt');
		exec ('rm /srv/files/tmp/invoice_' . $input->owner . '_' . $invoice->id . '.pdf');
	}

	$file_content = base64_encode(file_get_contents('/srv/files/tmp/export_invoices.zip'));
	exec ('rm /srv/files/tmp/export_invoices.zip');
	return array("code" => 200, "data" => $file_content);
};
?>