<?php
include_once 'libs/datatables.php';
$resource = json_decode('
{
	"id": { "type": "positive_integer", "field": "invoices_reminders.id", "post": ["ignored"], "patch": ["immutable"], "default": 0 },
	"invoice": { "type": "positive_integer", "field": "invoices_reminders.invoice", "post": ["required", "notnull", "notempty"], "patch": ["immutable"], "default": 0 },
	"date": { "type": "date", "field": "invoices_reminders.date", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["emptytodefault","nulltodefault"], "default": "' . date('Y-m-d') . '" },
	"type": { "type": "positive_integer", "field": "invoices_reminders.type", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": 10 },
	"type_description" : { "type": "string", "field": "invoices_reminders_types.value", "reference" : { "db" : "common.invoices_reminders_types", "id" : "invoices_reminders_types.id", "match" : "invoices_reminders.type" } }
}
', null, 512, JSON_THROW_ON_ERROR);

$get = function ()
{
	global $connection, $input, $resource;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->invoice_id = check('invoice_id', $input->path[3], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[5], 'strictly_positive_integer', false);

	if (exists($connection, 'user_' . $input->owner, 'partners', 'user', $input->user->id) OR is_admin($input->user->id))
		$restrictions = [];
	else
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'invoices/' . $input->invoice_id);
		if (in_array('read', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lire cette facture");
	}

	if (!exists($connection, 'user_' . $input->owner, 'invoices', 'id', $input->invoice_id))
		return array("code" => 400, "message" => "La facture spécifiée n'existe pas");
	
	if (isset($input->id))
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $input->id . '},{"field": "invoice", "type": "=", "value": ' . $input->invoice_id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_'. $input->owner, 'invoices_reminders');
		if (sizeof($results) == 0)
			return array("code" => 404, "message" => "cette relance n'existe pas");
		else
			return array("code" => 200, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
	else
	{
		$input->body = json_decode('{"filter": [{"field": "invoice", "type": "=", "value": ' . $input->invoice_id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_'. $input->owner, 'invoices_reminders', $restrictions);
		$last_row = (int)$connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		$last_page = $input->body->size > 0 ? ceil(max($last_row,1) / $input->body->size) : 1;
		for ($i=0; $i < sizeof($results); $i++)
			$results[$i]['restrictions'] = $restrictions['invoices/' . $results[$i]['id']] ?? array_diff((array)$restrictions['incomes'],['create']);
		$results = array_values(array_filter($results, function ($result) {return (!in_array('read', $result['restrictions']));}));
		return array("code" => 200, "data" => sanitize($resource, $results), "last_row" => $last_row, "last_page" => $last_page);
	}
};

$post = function ()
{
	global $connection, $input, $resource;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->invoice_id = check('invoice_id', $input->path[3], 'strictly_positive_integer', true);
	$input->body->invoice = $input->invoice_id;

	if (exists($connection, 'user_' . $input->owner, 'partners', 'user', $input->user->id) OR is_admin($input->user->id))
		$restrictions = [];
	else
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'invoices/' . $input->invoice_id);
		if (in_array('write', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier cette facture");
	}

	if (!exists($connection, 'user_' . $input->owner, 'invoices', 'id', $input->invoice_id))
		return array("code" => 400, "message" => "La facture spécifiée n'existe pas");

	check_input_body($resource, 'post');

	$query = datatables_insert($connection, $resource, 'user_' . $input->owner, 'invoices_reminders');
	if ($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $connection->lastInsertId() . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'invoices_reminders');
		return array("code" => 201, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);
};

$patch = function ()
{
	global $connection, $input, $resource;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->invoice_id = check('invoice_id', $input->path[3], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[5], 'strictly_positive_integer', true);

	check_input_body($resource, 'patch');

	if (exists($connection, 'user_' . $input->owner, 'partners', 'user', $input->user->id) OR is_admin($input->user->id))
		$restrictions = [];
	else 
	{
		$restrictions = get_restrictions($input->user->id, 'user_' . $input->owner, 'invoices/' . $input->invoice_id);
		if (in_array('write', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier cette facture");
	}

	if (!exists($connection, 'user_' . $input->owner, 'invoices_reminders', 'id', $input->id))
		return array("code" => 404, "message" => "Cette relance n'existe pas");

	$query = datatables_update($connection, $resource, 'user_' . $input->owner, 'invoices_reminders', $input->id);
	if($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $input->id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'invoices_reminders');
		return array("code" => 200, "data" => sanitize($resource, array_merge((array)$results[0], ['restrictions' => $restrictions])));
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);
};

$delete = function ()
{
	global $connection, $input, $resource;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->invoice_id = check('invoice_id', $input->path[3], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[5], 'strictly_positive_integer', true);

	if (exists($connection, 'user_' . $input->owner, 'partners', 'user', $input->user->id) OR is_admin($input->user->id))
		$restrictions = [];
	else 
	{
		$restrictions = get_restrictions($input->user->id, 'user_' . $input->owner, 'invoices/' . $input->invoice_id);
		if (in_array('write', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier cette facture");
	}

	if (!exists($connection, 'user_' . $input->owner, 'invoices_reminders', 'id', $input->id))
		return array("code" => 404, "message" => "Cette relance n'existe pas");

	$delete = $connection->query("DELETE FROM `user_" . $input->owner . "`.`invoices_reminders` WHERE id = '" . $input->id . "'");
	return array("code" => 200);
};
?>