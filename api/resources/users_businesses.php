<?php
$get = function ()
{
	global $connection, $input;
	auth();
	allowed_origins_only();

	$input->owner = check('id', $input->path[2], 'strictly_positive_integer', true);

	$businesses = $connection->query("SELECT * FROM `user_" . $input->owner . "`.`businesses`")->fetchAll(PDO::FETCH_OBJ);

	if (sizeof($businesses) == 0)
		return array("code" => 404);

	for($i=0; $i < sizeof($businesses); $i++)
	{
		$business_info = rest('https://api.' . $businesses[$i]->server . '/optimus-business/businesses/' . $businesses[$i]->business_id, 'GET', null, $_COOKIE['token']);
		$businesses[$i]->displayname = $business_info->data->displayname;
		$businesses[$i]->id = intval($businesses[$i]->business_id);
		unset($businesses[$i]->business_id);
	}
	
	return array("code" => 200, "data" => $businesses);
};
?>