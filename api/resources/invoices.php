<?php
include_once 'libs/datatables.php';
$resource = json_decode('
{
	"id": { "type": "positive_integer", "field": "invoices.id", "post": ["ignored"], "patch": ["immutable"], "default": 0 },
	"number": { "type": "string", "field": "invoices.number", "post": ["autogenerated"], "patch": ["immutable"], "default": 0 },
	"date": { "type": "date", "field": "invoices.date", "post": ["autogenerated"], "patch": ["immutable"], "default": "' . date('Y-m-d') . '" },
	"total_tax_exclusive": { "type": "decimal", "field": "invoices.total_tax_exclusive", "post": ["autogenerated"], "patch": ["immutable"], "default": "" },
	"total_tax_inclusive": { "type": "decimal", "field": "invoices.total_tax_inclusive", "post": ["autogenerated"], "patch": ["immutable"], "default": "" },
	"total_vat": { "type": "decimal", "field": "invoices.total_vat", "post": ["autogenerated"], "patch": ["immutable"], "default": "" },
	"total_paid" : { "type": "decimal", "field": "COALESCE((SELECT SUM(amount) FROM user_{owner}.invoices_payments WHERE invoice = invoices.id),0)", "virtual": true },
	"total_unpaid" : { "type": "decimal", "field": "(invoices.total_tax_inclusive - COALESCE((SELECT SUM(amount) FROM user_{owner}.invoices_payments WHERE invoice = invoices.id),0))", "virtual": true },
	"percent_paid" : { "type": "positive_integer", "field": "COALESCE(ROUND((SELECT SUM(amount) FROM user_{owner}.invoices_payments WHERE invoice = invoices.id) / invoices.total_tax_inclusive * 100), 0)", "virtual": true },
	"disbursements": { "type": "decimal", "field": "invoices.disbursements", "post": ["autogenerated"], "patch": ["immutable"], "default": "" },
	"vat_rate": { "type": "positive_integer", "field": "invoices.vat_rate", "post": ["required", "notnull", "notempty"], "patch": ["immutable"], "default": "" },
	"collectible": { "type": "boolean", "field": "invoices.collectible", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["notnull", "notempty"], "default": true },
	"template": { "type": "string", "field": "invoices.template", "post": ["notnull", "notempty"], "patch": ["notnull", "notempty"], "default": "" },
	"client_endpoint": { "type": "string", "field": "invoices.client_endpoint", "post": ["required", "notnull", "notempty"], "patch": ["immutable"], "default": null },
	"client_server": { "type": "domain", "field": "invoices.client_server", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["immutable"], "default": "' . getenv('DOMAIN') . '" },
	"client_owner": { "type": "positive_integer", "field": "invoices.client_owner", "post": ["required", "notnull", "notempty"], "patch": ["immutable"], "default": "" },
	"client_id": { "type": "positive_integer", "field": "invoices.client_id", "post": ["required", "notnull", "notempty"], "patch": ["immutable"], "default": 0 },
	"client_displayname": { "type": "string", "field": "invoices.client_displayname", "post": ["autogenerated"], "patch": ["immutable"], "default": "" },
	"client_siren": { "type": "string", "field": "invoices.client_siren", "post": ["autogenerated"], "patch": ["immutable"], "default": "" },
	"client_address": { "type": "text", "field": "invoices.client_address", "post": ["autogenerated"], "patch": ["immutable"], "default": "" },
	"client_zipcode": { "type": "string", "field": "invoices.client_zipcode", "post": ["autogenerated"], "patch": ["immutable"], "default": "" },
	"client_city": { "type": "string", "field": "invoices.client_city", "post": ["autogenerated"], "patch": ["immutable"], "default": "" },
	"client_city_name": { "type": "string", "field": "invoices.client_city_name", "post": ["autogenerated"], "patch": ["immutable"], "default": "" },
	"client_country": { "type": "positive_integer", "field": "invoices.client_country", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["immutable"], "default": "" },
	"client_country_name": { "type": "string", "field": "invoices.client_country_name", "post": ["autogenerated"], "patch": ["immutable"], "default": "" },
	"details_endpoint": { "type": "string", "field": "invoices.reference_endpoint", "post": ["required", "notnull", "notempty"], "patch": ["immutable"], "default": null },
	"details_server": { "type": "domain", "field": "invoices.details_server", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["immutable"], "default": "' . getenv('DOMAIN') . '" },
	"details_owner": { "type": "positive_integer", "field": "invoices.details_owner", "post": ["required", "notnull", "notempty"], "patch": ["immutable"], "default": "" },
	"reference_endpoint": { "type": "string", "field": "invoices.reference_endpoint", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["immutable"], "default": null },
	"reference_server": { "type": "domain", "field": "invoices.reference_server", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["immutable"], "default": "' . getenv('DOMAIN') . '" },
	"reference_owner": { "type": "positive_integer", "field": "invoices.reference_owner", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["immutable"], "default": null },
	"reference_id": { "type": "positive_integer", "field": "invoices.reference_id", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["immutable"], "default": null },
	"reference_displayname": { "type": "string", "field": "invoices.reference_displayname", "post": ["autogenerated"], "patch": ["immutable"], "default": null },
	"properties": { "type": "json", "field": "invoices.properties", "post": ["undefinedtodefault", "nulltodefault", "emptytodefault"], "patch": ["emptytodefault"], "default": "{}" },
	"notes": { "type": "text", "field": "invoices.notes", "post": ["emptytonull"], "patch": ["emptytonull"], "default": null },
	"displayname" : { "type" : "string", "field": "CASE {invoices_numbering_format} WHEN 2 THEN SUBSTRING(invoices.number,4,8) ELSE number END", "virtual": true },
	"owner_displayname" : { "type": "string", "field": "owner_details.displayname", "reference" : { "db" : "server.users AS owner_details", "id" : "owner_details.id", "match" : "invoices.details_owner" } }
}
', null, 512, JSON_THROW_ON_ERROR);


$get = function ()
{
	global $connection, $input, $resource;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', false);
	$resource->total_paid->field = str_replace('{owner}', $input->owner, $resource->total_paid->field);
	$resource->total_unpaid->field = str_replace('{owner}', $input->owner, $resource->total_unpaid->field);
	$resource->percent_paid->field = str_replace('{owner}', $input->owner, $resource->percent_paid->field);

	$business = $connection->query("SELECT invoices_numbering_format FROM `server`.`businesses` WHERE id = '" . $input->owner . "'")->fetch(PDO::FETCH_OBJ);
	$resource->displayname->field = str_replace('{invoices_numbering_format}', $business->invoices_numbering_format, $resource->displayname->field);

	if (exists($connection, 'user_' . $input->owner, 'partners', 'user', $input->user->id) OR is_admin($input->user->id))
		$restrictions = [];
	else if (isset($input->id))
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'invoices/' . $input->id);
		if (in_array('read', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lire cette facture");
	}
	else
	{
		$restrictions = get_restrictions_list($input->user->id, $input->owner, 'invoices');
		if (sizeof($restrictions) > 0 AND array_count_values(array_column($restrictions, 0))['read'] == sizeof($restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour lister les factures de cette structure");
	}

	if (isset($input->id))
	{
		$input->body = json_decode('{"references": true, "filter": [{"field": "id", "type": "=", "value": ' . $input->id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_'. $input->owner, 'invoices');
		if (sizeof($results) == 0)
			return array("code" => 404, "message" => "Cette facture n'existe pas");
		else
			return array("code" => 200, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
	//REQUETE SUR TOUTES LES FACTURES AU FORMAT DATATABLES
	else 
	{	
		$results = datatable_request($connection, $resource, 'user_'. $input->owner, 'invoices', $restrictions);
		$last_row = (int)$connection->query('SELECT FOUND_ROWS()')->fetchColumn();
		$last_page = $input->body->size > 0 ? ceil(max($last_row,1) / $input->body->size) : 1;
		for ($i=0; $i < sizeof($results); $i++)
			$results[$i]['restrictions'] = $restrictions['invoices/' . $results[$i]['id']] ?? array_diff((array)$restrictions['invoices'],['create']);
			
		$results = array_values(array_filter($results, function ($result) {return (!in_array('read', $result['restrictions']));}));
		return array("code" => 200, "data" => sanitize($resource, $results), "last_row" => $last_row, "last_page" => $last_page);
	}
};


$post = function ()
{
	global $connection, $input, $resource;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$resource->total_paid->field = str_replace('{owner}', $input->owner, $resource->total_paid->field);
	$resource->total_unpaid->field = str_replace('{owner}', $input->owner, $resource->total_unpaid->field);
	$resource->percent_paid->field = str_replace('{owner}', $input->owner, $resource->percent_paid->field);

	$business = $connection->query("SELECT invoices_numbering_format FROM `server`.`businesses` WHERE id = '" . $input->owner . "'")->fetch(PDO::FETCH_OBJ);
	$resource->displayname->field = str_replace('{invoices_numbering_format}', $business->invoices_numbering_format, $resource->displayname->field);

	if (exists($connection, 'user_' . $input->owner, 'partners', 'user', $input->user->id) OR is_admin($input->user->id))
		$restrictions = [];
	else
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'invoices');
		if (in_array('create', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour créer une facture");
	}

	$resource->details_ids = (object)array("type"=>"text", "field"=>"invoices.id", "post"=>["required", "notnull", "notempty"], "patch"=>["ignored"]);
	check_input_body($resource, 'post');

	//GENERATION DU NUMERO DE FACTURE
	if ($connection->query("SELECT id FROM `user_" . $input->owner . "`.`invoices`")->rowCount() == 0)
	{
		if (!isset($input->body->number))
			return array("code" => 418, "message" => "Pour créer la première facture, il est indispensable de préciser son numéro");
		else
		{
			if ($business->invoices_numbering_format == 1)
			{
				$input->body->number = check('number', $input->body->number, 'string', true);
			}
			else
			{
				$input->body->number = check('number', $input->body->number, 'strictly_positive_integer', true);
				$input->body->number = date('y') . '-' . str_pad($input->body->number, 5, "0", STR_PAD_LEFT);
			}
		}
	}
	else
	{
		$last_number = $connection->query("SELECT `number` FROM `user_" . $input->owner . "`.`invoices` WHERE number LIKE '" . date('y') . "-%' ORDER BY id DESC LIMIT 1")->fetchObject();
		$input->body->number = date('y') . '-' . str_pad(intval(substr($last_number->number,3))+1, 5, "0", STR_PAD_LEFT);
	}
	
	$input->body->date = date('Y-m-d');

	//VERIFICATION DES PRESTATIONS A FACTURER
	$details_endpoint = str_replace('{owner}', $input->body->details_owner, $input->body->details_endpoint);
	$details_endpoint = str_replace('{reference}', $input->body->reference_id, $details_endpoint);
	if (!is_array($input->body->details_ids))
		return array("code" => 400, "message" => "Vous devez fournir un tableau nommé 'details_ids' contenant les 'id' des prestations facturées");
	$details = rest('https://api.' . $input->body->details_server . $details_endpoint, 'GET', '{"filter":[{"field":"invoice_number","type":"isnull"}]}', $_COOKIE['token']);
	if ($details->code != 200)
		return array("code" => $details->code, "message" => $details->message);
	foreach($details->data as $key => $detail)
		if (!in_array($detail->id, $input->body->details_ids))
			unset($details->data[$key]);
		else
			$details_id[] = $detail->id;
	foreach($input->body->details_ids as $id)
		if(!in_array($id, $details_id))
			return array("code" => 400, "message" => "La prestation n° " . $id . " n'existe pas ou a déjà été facturée");
	$details = $details->data;
	
	//CALCUL DES MONTANTS
	foreach($details as $detail)
		if ($detail->category == 1 OR $detail->category == 2)
			$input->body->total_tax_exclusive += round($detail->quantity * $detail->price, 2, PHP_ROUND_HALF_UP);
		else if ($detail->category == 3)
			$input->body->disbursements += round($detail->quantity * $detail->price, 2, PHP_ROUND_HALF_UP);
	$vat_rate = $connection->query("SELECT rate FROM `common`.`vat_rates` WHERE id = '" . $input->body->vat_rate . "'")->fetch(PDO::FETCH_OBJ);
	if (!$vat_rate->rate)
		return array("code" => 400, "message" => "Le taux de tva renseigné n'a pas été trouvé dans la base");
	$input->body->total_vat = round($input->body->total_tax_exclusive * $vat_rate->rate / 100, 2, PHP_ROUND_HALF_UP);
	$input->body->total_tax_inclusive = $input->body->total_tax_exclusive + $input->body->disbursements + $input->body->total_vat;
	
	//RECUPERATION DES COORDONNEES DU CLIENT
	$client_endpoint = str_replace('{owner}', $input->body->client_owner, $input->body->client_endpoint);
	$client_endpoint = str_replace('{id}', $input->body->client_id, $client_endpoint);
	$client = rest('https://api.' . $input->body->client_server . $client_endpoint, 'GET', '{}', $_COOKIE['token']);
	if ($client->code != 200)
		return array("code" => $client->code, "message" => $client->message);
	else
		$client = $client->data;
	
	$input->body->client_displayname = $client->displayname;
	//if (($client->type == 30 OR $client->type == 40) AND $client->siren == '')
		//return array("code" => 400, "message" => "Le numéro SIREN du client n'est pas renseignée");
	$input->body->client_siren = $client->siren;
	if ($client->address == "")
		return array("code" => 400, "message" => "L'adresse du client spécifié n'est pas renseignée");
	$input->body->client_address = $client->address;
	if ($client->zipcode == "" AND !stripos($client->address,'cedex'))
		return array("code" => 400, "message" => "Le code postal du client spécifié n'est pas renseignée");
	$input->body->client_zipcode = $client->zipcode;
	if ($client->city_name == "" AND !stripos($client->address,'cedex'))
		return array("code" => 400, "message" => "La ville du client spécifié n'est pas renseignée");
	$input->body->client_city = $client->city;
	$input->body->client_city_name = $client->city_name;
	if ($client->country == 0)
		return array("code" => 400, "message" => "Le pays de résidence du client n'est pas renseigné");
	$input->body->client_country = $client->country;
	$country = $connection->query("SELECT value FROM `common`.`pays` WHERE id = '" . $client->country . "'")->fetch(PDO::FETCH_OBJ);
	$input->body->client_country_name = $country->value;
	
	//RECUPERATION DES INFORMATIONS DU DOSSIER
	$reference_endpoint = str_replace('{owner}', $input->body->reference_owner, $input->body->reference_endpoint);
	$reference_endpoint = str_replace('{id}', $input->body->reference_id, $reference_endpoint);
	$reference = rest('https://api.' . $input->body->reference_server . $reference_endpoint, 'GET', '{}', $_COOKIE['token']);
	if ($reference->code != 200)
		return array("code" => $reference->code, "message" => $reference->message);
	else
		$input->body->reference_displayname = $reference->data->displayname;

	$details_id = $input->body->details_ids;
	unset($resource->details_ids);
	check_input_body($resource, 'post');

	$query = datatables_insert($connection, $resource, 'user_' . $input->owner, 'invoices');
	if ($query->execute())
	{
		$invoice_id = $connection->lastInsertId();
		//VERROUILLAGE DES PRESTATIONS FACTUREES
		$invoice_business = $connection->query("SELECT displayname FROM `server`.`businesses` WHERE id = '" . $input->owner . "'")->fetch(PDO::FETCH_OBJ);
		if ($business->invoices_numbering_format == 1)
			$invoice_displayname = str_pad($input->body->number, 5, "0", STR_PAD_LEFT);
		else
			$invoice_displayname = str_pad(intval(substr($last_number->number,3))+1, 5, "0", STR_PAD_LEFT);
		$detail_data = array
			(
				"invoice_server" => getenv('DOMAIN'),
				"invoice_business_displayname" => $invoice_business->displayname,
				"invoice_owner" => $input->owner,
				"invoice_id" => $invoice_id,
				"invoice_number" => $invoice_displayname
			);
		foreach($details_id as $id)
		{
			$detail = rest('https://api.' . $input->body->details_server . $details_endpoint . '/' . $id, 'PATCH', json_encode($detail_data), $_COOKIE['token']);
			if ($detail->code != 200)
				return array("code" => $detail->code, "message" => $detail->message);
		}
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $invoice_id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'invoices');

		return array("code" => 201, "data" => sanitize($resource, array_merge($results[0], ['restrictions' => $restrictions])));
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);
};


$patch = function ()
{
	global $connection, $input, $resource;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('id', $input->path[3], 'strictly_positive_integer', true);
	$resource->total_paid->field = str_replace('{owner}', $input->owner, $resource->total_paid->field);
	$resource->total_unpaid->field = str_replace('{owner}', $input->owner, $resource->total_unpaid->field);
	$resource->percent_paid->field = str_replace('{owner}', $input->owner, $resource->percent_paid->field);

	$business = $connection->query("SELECT invoices_numbering_format FROM `server`.`businesses` WHERE id = '" . $input->owner . "'")->fetch(PDO::FETCH_OBJ);
	$resource->displayname->field = str_replace('{invoices_numbering_format}', $business->invoices_numbering_format, $resource->displayname->field);

	check_input_body($resource, 'patch');

	if (exists($connection, 'user_' . $input->owner, 'partners', 'user', $input->user->id) OR is_admin($input->user->id))
		$restrictions = [];
	else 
	{
		$restrictions = get_restrictions($input->user->id, 'user_' . $input->owner, 'invoices/' . $input->id);
		if (in_array('write', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour modifier cette facture");
	}

	if (!exists($connection, 'user_' . $input->owner, 'invoices', 'id', $input->id))
		return array("code" => 404, "message" => "Cette facture n'existe pas");

	$query = datatables_update($connection, $resource, 'user_' . $input->owner, 'invoices', $input->id);
	if($query->execute())
	{
		$input->body = json_decode('{"filter": [{"field": "id", "type": "=", "value": ' . $input->id . '}]}', null, 512, JSON_THROW_ON_ERROR);
		$results = datatable_request($connection, $resource, 'user_' . $input->owner, 'invoices');
		return array("code" => 200, "data" => sanitize($resource, array_merge((array)$results[0], ['restrictions' => $restrictions])));
	}
	else
		return array("code" => 400, "message" => $query->errorInfo()[2]);
};


$delete = function ()
{
	return array("code" => 403, "message" => "conformément aux règles légales, une facture ne peut pas être supprimée.\nElle ne peut qu'être annulée par un avoir.");
};
?>