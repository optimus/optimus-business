<?php
include_once 'libs/datatables.php';

$get = function ()
{
	global $connection, $input, $resource;
	auth();
	allowed_origins_only();

	$input->owner = check('owner', $input->path[1], 'strictly_positive_integer', true);
	$input->id = check('owner', $input->path[3], 'strictly_positive_integer', true);

	if (exists($connection, 'user_' . $input->owner, 'partners', 'user', $input->user->id) OR is_admin($input->user->id))
		$restrictions = [];
	else
	{
		$restrictions = get_restrictions($input->user->id, $input->owner, 'invoices/' . $input->id);
		if (in_array('read', $restrictions))
			return array("code" => 403, "message" => "Vous n'avez pas les autorisations suffisantes pour générer cette facture");
	}

	$invoice = $connection->query("SELECT * FROM `user_" . $input->owner . "`.invoices WHERE id = '" . $input->id . "'")->fetch(PDO::FETCH_OBJ);
	
	if (!file_exists('/srv/files/' .  get_user_email($input->owner) . '/modeles/factures/' . $invoice->template))
		return array("code" => 400, "message" => "Le modèle " . $invoice->template . " n'existe pas");
	
	$business = $connection->query("SELECT * FROM `server`.`businesses` WHERE id = '" . $input->owner . "'")->fetch(PDO::FETCH_OBJ);
	
	$details_endpoint = str_replace('{owner}', $invoice->details_owner, $invoice->details_endpoint);
	$details_endpoint = str_replace('{reference}', $invoice->reference_id, $details_endpoint);
	$details_filter = '{"sort": [{"field":"date","dir":"asc"}],"filter": [{"field": "invoice_server", "type": "=", "value": "' . getenv('DOMAIN') . '"}, {"field": "invoice_owner", "type": "=", "value": "' . $input->owner . '"}, {"field": "invoice_id", "type": "=", "value": "' . $input->id . '"}]}';
	$details = rest('https://api.' . $invoice->details_server . $details_endpoint, 'GET', $details_filter, $_COOKIE['token'])->data;

	$reference_endpoint = str_replace('{owner}', $invoice->reference_owner, $invoice->reference_endpoint);
	$reference_endpoint = str_replace('{id}', $invoice->reference_id, $reference_endpoint);
	$reference = rest('https://api.' . $invoice->reference_server . $reference_endpoint, 'GET', '{}', $_COOKIE['token'])->data;
	
	$client_endpoint = str_replace('{owner}', $invoice->client_owner, $invoice->client_endpoint);
	$client_endpoint = str_replace('{id}', $invoice->client_id, $client_endpoint);
	$client = rest('https://api.' . $invoice->client_server . $client_endpoint, 'GET', '{}', $_COOKIE['token'])->data;
	
	$pays = $connection->query("SELECT value FROM `common`.`pays` WHERE id = '" . $client->country . "'")->fetch(PDO::FETCH_OBJ);

	$vat_rate = $connection->query("SELECT rate FROM `common`.`vat_rates` WHERE id = '" . $invoice->vat_rate . "'")->fetch(PDO::FETCH_OBJ);

	$categories_list = $connection->query("SELECT id, value FROM `common`.`diligences_categories`")->fetchAll(PDO::FETCH_OBJ);
	foreach($categories_list as $categorie)
		$categories[$categorie->id] = $categorie->value;
	$subcategories_list = $connection->query("SELECT id, value FROM `common`.`diligences_subcategories`")->fetchAll(PDO::FETCH_OBJ);
	foreach($subcategories_list as $subcategorie)
		$subcategories[$subcategorie->id] = $subcategorie->value;

	foreach($details as $detail)
		if ($detail->category == 1)
			$total_fees += $detail->quantity * $detail->price;
		else if ($detail->category == 2)
			$total_expenses += $detail->quantity * $detail->price;

	$odt = new ZipArchive;
	if ($odt->open('/srv/files/' .  get_user_email($input->owner) . '/modeles/factures/' . $invoice->template) === TRUE) 
	{
		$odt->extractTo('/srv/files/tmp/invoice_' . $input->owner . '_' . $invoice->id);
		$odt->close();
		$odt_content = file_get_contents('/srv/files/tmp/invoice_' . $input->owner . '_' . $invoice->id . '/content.xml');

		$odt_content = str_replace('{CLIENT_NAME}', htmlspecialchars(strtoupper($invoice->client_displayname)), $odt_content);

		$odt_content = str_replace('{CLIENT_ADDRESS}',str_replace("\n","<text:line-break/>", htmlspecialchars($invoice->client_address)), $odt_content);
			
		if (preg_match('/cedex/i', $invoice->client_address) > 0)
		{
		 	$odt_content = str_replace('{CLIENT_ZIPCODE}', '', $odt_content);
			$odt_content = str_replace('{CLIENT_CITY}', '', $odt_content);
		}
		else
		{
			$odt_content = str_replace('{CLIENT_ZIPCODE}', $invoice->client_zipcode, $odt_content);
			$odt_content = str_replace('{CLIENT_CITY}', strtoupper($invoice->client_city_name), $odt_content);
		}
		
		$odt_content = str_replace('{CLIENT_COUNTRY}', strtoupper($pays->value), $odt_content);

		$odt_content = str_replace('{CLIENT_SIREN}', $client->siren  ?: '', $odt_content);

		$odt_content = str_replace('{VAT_NUMBER}', $client->tva ?: '', $odt_content);

		$odt_content = str_replace('{REFERENCE_NAME}', $invoice->reference_displayname ? htmlspecialchars($invoice->reference_displayname) : '', $odt_content);
		$odt_content = str_replace('{REFERENCE_NUMBER}', $reference->numero ?: '', $odt_content);

		$odt_content = str_replace('{INVOICE_DATE}',date('d/m/Y', strtotime($invoice->date)),$odt_content);
		
		if ($invoice->total_tax_inclusive >= 0)
		{
			$odt_content = str_replace('{FACTURE_TYPE}', 'FACTURE', $odt_content);
			$odt_content = str_replace('{INVOICE_TYPE}', 'INVOICE', $odt_content);
			$odt_content = str_replace('{RECHNUNG_TYP}', 'RECHNUNG', $odt_content);
		}
		else
		{
			$odt_content = str_replace('{FACTURE_TYPE}', 'AVOIR', $odt_content);
			$odt_content = str_replace('{INVOICE_TYPE}', 'CREDIT NOTE', $odt_content);
			$odt_content = str_replace('{RECHNUNG_TYP}', 'GUTSCHRIFT', $odt_content);
		}
			
		if ($business->invoices_numbering_format == 1)
			$odt_content = str_replace('{INVOICE_NUMBER}', $invoice->number, $odt_content);
		else if ($business->invoices_numbering_format == 2)
			$odt_content = str_replace('{INVOICE_NUMBER}', substr($invoice->number,3), $odt_content);

		$odt_content = str_replace('{FEES}', number_format($total_fees,2,',',' '), $odt_content);
		$odt_content = str_replace('{EXPENSES}', number_format($total_expenses,2,',',' '), $odt_content);
		$odt_content = str_replace('{SUBTOTAL}', number_format($invoice->total_tax_exclusive,2,',',' '), $odt_content);
		$odt_content = str_replace('{VAT}', number_format($invoice->total_vat,2,',',' '), $odt_content);
		$odt_content = str_replace('{DISBURSEMENTS}', number_format($invoice->disbursements,2,',',' '), $odt_content);
		$odt_content = str_replace('{GRAND_TOTAL}', number_format($invoice->total_tax_inclusive,2,',',' '), $odt_content);

		if ($invoice->vat_rate==1 or $invoice->vat_rate==20 or $invoice->vat_rate==60 or $invoice->vat_rate==70)
		{
			$odt_content = str_replace('{TAUX_TVA}', $vat_rate->rate . ' %', $odt_content);
			$odt_content = str_replace('{TVA_RATE}', $vat_rate->rate . ' %', $odt_content);
			$odt_content = str_replace('{MWST_PROZENT}', $vat_rate->rate . ' %', $odt_content);
		}	
		else if ($invoice->vat_rate==30 OR $invoice->vat_rate==40)
		{
			$odt_content = str_replace('{TAUX_TVA}', 'Non applicable - ART. 259B CGI', $odt_content);
			$odt_content = str_replace('{TVA_RATE}', 'Not applicable - ART. 259B CGI', $odt_content);
			$odt_content = str_replace('{MWST_PROZENT}', 'Mehrwertsteuerfrei - Artikel 259B CGI', $odt_content);
		}
		else if ($invoice->vat_rate==50)
		{
			$odt_content = str_replace('{TAUX_TVA}', 'Non applicable - ART. 293B CGI', $odt_content);
			$odt_content = str_replace('{TVA_RATE}', 'Not applicable - ART. 293B CGI', $odt_content);
			$odt_content = str_replace('{MWST_PROZENT}', 'Mehrwertsteuerfrei - Artikel 259B CGI', $odt_content);
		}

		$odt_content = str_replace('{DETAIL_AT_BACK}','See detailed time sheet at the back',$odt_content);
		$odt_content = str_replace('{DETAIL_AU_VERSO}','Détail des diligences au verso',$odt_content);
		$odt_content = str_replace('{DETAIL_AUF_DER_RUCKZEITE}','Detaillierte Zustand auf der Rückseite',$odt_content);

		$odt_content = str_replace('{IBAN}', $business->iban, $odt_content);
		$odt_content = str_replace('{BIC}', $business->bic, $odt_content);

		$odt_content_table_row_odd = substr($odt_content, strpos($odt_content, '<table:table-row table:style-name="Table2.2">'));
		$odt_content_table_row_odd = substr($odt_content_table_row_odd, 0, strpos($odt_content_table_row_odd, '</table:table-row>') + 18);

		$odt_content_table_row_even = substr($odt_content, strpos($odt_content, '<table:table-row table:style-name="Table2.3">'));
		$odt_content_table_row_even = substr($odt_content_table_row_even, 0, strpos($odt_content_table_row_even, '</table:table-row>') + 18);

		foreach($details as $detail)
		{
			if ($x++ % 2!=0)
				$new_row = $odt_content_table_row_odd;
			else
				$new_row = $odt_content_table_row_even;

			$new_row = str_replace('{DATE}', date('d/m/Y',strtotime($detail->date)), $new_row);
			$new_row = str_replace('{DESCRIPTION}', str_replace("\n","<text:line-break/>", htmlspecialchars($detail->description)), $new_row);
			$new_row = str_replace('{CATEGORY}', $categories[$detail->category], $new_row);
			$new_row = str_replace('{TYPE}', $subcategories[$detail->type], $new_row);
			$new_row = str_replace('{QUANTITY}', $detail->quantity, $new_row);
			$new_row = str_replace('{PRICE}', $detail->price, $new_row);
			$new_row = str_replace('{TOTAL}', number_format($detail->quantity * $detail->price,2,".",""), $new_row);

			$allrows .= $new_row;
		}

		$odt_content = str_replace($odt_content_table_row_odd, $allrows, $odt_content);
		$odt_content = str_replace($odt_content_table_row_even, '', $odt_content);


		file_put_contents('/srv/files/tmp/invoice_' . $input->owner . '_' . $invoice->id . '/content.xml', $odt_content);
		$zip = new ZipArchive();
		$zip->open('/srv/files/tmp/invoice_' . $input->owner . '_' . $invoice->id . '.odt', ZipArchive::CREATE | ZipArchive::OVERWRITE);
		$files = new RecursiveIteratorIterator (new RecursiveDirectoryIterator('/srv/files/tmp/invoice_' . $input->owner . '_' . $invoice->id), RecursiveIteratorIterator::LEAVES_ONLY);
		foreach ($files as $name => $file)
			if (!$file->isDir())
			{
				$filePath = $file->getRealPath();
				$relativePath = substr($filePath, strlen('/srv/files/tmp/invoice_' . $input->owner . '_' . $invoice->id) + 1);
				$zip->addFile($filePath, $relativePath);
			}
		if (!$zip->close())
			return array("code" => 400, "message" => "Une erreur est survenue");
	}
	
	$file_content = base64_encode(file_get_contents('/srv/files/tmp/invoice_' . $input->owner . '_' . $invoice->id . '.odt'));
	exec ('rm -R /srv/files/tmp/invoice_' . $input->owner . '_' . $invoice->id);
	exec ('rm /srv/files/tmp/invoice_' . $input->owner . '_' . $invoice->id . '.odt');
	
	return array("code" => 200, "data" => $file_content, "authorizations" => $authorizations);
};
?>