export default class OptimusBusinessEditor
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		if (admin_only()) return

		loader(this.target)
		let response = await rest('https://' + this.server + '/optimus-business/businesses/' + this.id)
		if (response.code == 404)
			return load('/404.html', main) && loader(this.target, false)
		await load('/services/optimus-business/businesses/editor.html', main)
		main.querySelector('.title').innerText = response.data.displayname.toUpperCase()

		if (response.data?.restrictions?.includes('delete'))
			this.target.querySelector('.delete-button').classList.add('is-hidden')

		let tabs =
			[
				{
					id: "tab_general",
					text: "Général",
					link: "/services/optimus-business/businesses/general.js",
					default: true,
					position: 100
				},
				{
					id: "tab_partners",
					text: "Associés",
					link: "/services/optimus-business/businesses/partners.js",
					position: 200
				},
				{
					id: "tab_accounting",
					text: "Comptabilité",
					link: "/services/optimus-business/businesses/accounting.js",
					position: 300
				},
				{
					id: "tab_bankaccounts",
					text: "Comptes bancaires",
					link: "/services/optimus-business/businesses/bankaccounts.js",
					position: 400
				},
				{
					id: "tab_files",
					text: "Fichiers",
					link: "/services/optimus-business/businesses/files.js",
					position: 500
				},
				{
					id: "tab_data",
					text: "Données",
					link: "/services/optimus-business/businesses/data.js",
					position: 600
				}
			]

		let owner_services = (this.owner != store.user.id) ? await rest(store.user.server + '/optimus-base/users/' + this.owner + '/services').then(response => response.data.map(service => service.name)) : store.services.map(service => service.name)
		store.services.map(service => 
		{
			if (owner_services.includes(service.name) && service.optimus_business_businesses_tabs)
				tabs = tabs.concat(service.optimus_business_businesses_tabs)
		})

		await load('/components/optimus_tabs.js', this.target,
			{
				router: true,
				tabs_container: document.getElementById('business_editor_tabs'),
				content_container: document.getElementById('business_editor_tabs_container'),
				tabs: tabs,
				params: response
			})

		document.querySelector('.delete-button').onclick = () => modal.open('/services/optimus-business/businesses/delete_confirmation_modal.js')

		loader(this.target, false)
	}
}