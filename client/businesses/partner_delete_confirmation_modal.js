export default class OptimusBusinessEditorPartnerDeleteModal
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
	}

	async init()
	{
		if (admin_only()) return

		await load('/services/optimus-business/businesses/partner_delete_confirmation_modal.html', this.target)

		modal.querySelector('.delete-button').onclick = () =>
		{
			rest(this.server + '/optimus-business/' + this.params.entreprise_id + '/partners/' + this.params.partner_id, 'DELETE', null, 'toast')
				.then(response => 
				{
					if (response.code == 200)
					{
						if (document.getElementById('partner_' + this.params.partner_id))
							document.getElementById('partner_' + this.params.partner_id).parentNode.remove()
						modal.close()
						this.params.template.classList.add('animate__animated', 'animate__zoomOut', 'animate__faster')
						this.params.template.addEventListener('animationend', () => template.remove())
					}
				})
		}

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}
}