export default class OptimusBusinessEditorBankAccount
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		if (admin_only()) return

		await load('/services/optimus-business/businesses/data.html', this.target, null, true)
		await load('/components/optimus_form.js', this.target,
			{
				start_date: new Date().toISOString().split('-')[0] + '-01-01',
				end_date: new Date().toISOString().split('T')[0]
			}, true)

		this.target.querySelector('.export_invoices_button').onclick = () => modal.open('/services/optimus-business/businesses/data_export_invoices_modal.js', false, { start_date: this.target.querySelector('.start_date').value, end_date: this.target.querySelector('.end_date').value })
	}
}
