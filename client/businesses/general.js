export default class OptimusBusinessEditorGeneral
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.id = store.queryParams.id
	}

	async init()
	{
		if (admin_only()) return


		if (!store.queryParams.id)
			return this.target.innerHTML = "Aucun identifiant (id) n'a été renseigné"

		await load('/services/optimus-business/businesses/general.html', this.target, null, true)
		loader(this.target, true, 'list')
		const inputCollection = document.querySelectorAll("input,select,textarea")
		for (const el of inputCollection)
		{
			el.setAttribute('data-endpoint', '{server}/optimus-business/businesses/{id}')
			el.setAttribute('data-field', el.id)
		}
		const response = await rest('https://' + this.server + '/optimus-business/businesses/' + this.id)

		await populate(document.getElementById('country'), store.user.server + '/optimus-contacts/constants/pays', true, false)
		await populate(document.getElementById('type'), store.user.server + '/optimus-contacts/constants/company_types_lvl3', true, true)

		await load('/components/optimus_form.js', this.target, response.data)

		Promise.all
			(
				[
					change_country(false),
					get_cities(document.getElementById('city'), response.data.zipcode, response.data.city, false),
				]
			)
			.then(values => loader(this.target, false, 'list'))

		document.getElementById('name')?.addEventListener("blur", evt => document.querySelector('.title').innerText = evt.target.value.toUpperCase())

		document.getElementById('zipcode').addEventListener("change", evt => get_cities(document.getElementById('city'), evt.target.value, null, true))

		document.getElementById('city').addEventListener("change", function (evt)
		{
			if (evt.target.options.length >= 1)
			{
				document.getElementById('city_name').value = evt.target.options[evt.target.selectedIndex].text
				rest(store.user.server + '/optimus-business/businesses/' + store.queryParams.id, 'PATCH', { city_name: document.getElementById('city_name').value })
			}
			this.setCustomValidity('')
		})

		document.getElementById('country').addEventListener("change", () => change_country())

		document.querySelectorAll('.rne-import-button')[0].addEventListener("click", evt => get_rne_infos(document.querySelectorAll('.siren')[0].value))

		document.querySelector('.undo-button').addEventListener("click", function (evt) 
		{
			setTimeout(function () 
			{
				change_country(document.getElementById('country').value)
				get_cities(document.getElementById('city'), document.getElementById('zipcode').value, document.getElementById('city').value, false)
			}, 150)
		})

		async function get_cities(obj, zipcode, existingCity, save)
		{
			let prefix = obj.id.includes('birth_') ? 'birth_' : ''

			if (document.getElementById(prefix + 'country').value != 74)
				return false

			obj.options.length = 0

			if (!zipcode)
				return false

			if (zipcode == '')
			{
				document.getElementById(obj.id + '_name').value = null
				rest(store.user.server + '/optimus-business/businesses/' + store.queryParams.id, 'PATCH', { [obj.id]: null, [obj.id + '_name']: null })
				return false
			}

			return rest(store.user.server + '/optimus-contacts/constants/communes', 'GET', { filter: [{ field: "code_postal", type: "=", value: zipcode }, { field: "ancien_nom", type: "=", value: " " }], columns: ["nom", "commune_insee"] })
				.then((results) =>
				{
					const cities = results.data

					if (cities?.length == 0)
					{
						document.getElementById(prefix + 'zipcode').setCustomValidity('Ce code postal est inconnu en France. Veuillez choisir un autre pays ou modifier votre saisie')
						document.getElementById(prefix + 'zipcode').reportValidity()
						return false
					}

					if (cities?.length == 1)
					{
						let opt = document.createElement("option")
						opt.value = cities[0].commune_insee
						opt.text = cities[0].nom
						obj.add(opt)

						if (save === true)
							obj.dispatchEvent(new Event('change'))
					}
					else if (cities.length > 1)
					{
						obj.add(document.createElement("option"))
						for (let city of cities)
						{
							let opt = document.createElement("option")
							opt.value = city.commune_insee
							opt.text = city.nom
							obj.add(opt)
						}
						if (existingCity)
						{
							obj.value = existingCity
							if (save === true)
								obj.dispatchEvent(new Event('change'))
						}
						else if (save === true)
						{
							setTimeout(() => 
							{
								obj.setCustomValidity('Veuillez selectionner la ville dans le menu déroulant')
								obj.reportValidity()
							}, 150)
							obj.dispatchEvent(new Event('change'))
						}
					}
					return true
				})
		}

		async function get_rne_infos(siren)
		{
			if (!siren)
				return document.querySelectorAll('.siren').forEach(input => 
				{
					input.setCustomValidity('Le numéro SIREN doit être renseigné')
					input.reportValidity()
				})

			if (siren.length != 9)
				return document.querySelectorAll('.siren').forEach(input => 
				{
					input.setCustomValidity('Le numéro doit contenir 9 chiffres sans espace')
					input.reportValidity()
				})

			const sireneCall = await fetch('https://recherche-entreprises.api.gouv.fr/search?q=' + siren)

			if (!sireneCall.ok)
				return optimusToast('Le Répertoire National des Entreprises ne répond pas. Veuillez réessayer ultérieurement', 'is-warning')

			const sirene = await sireneCall.json()

			if (sirene.results.length == 0)
				return optimusToast('Aucun résultat pour ce numéro SIREN', 'is-warning')

			const rne_infos = sirene.results[0]
			rest(store.user.server + '/optimus-business/businesses/' + store.queryParams.id, 'PATCH',
				{
					name: rne_infos.complements.est_entrepreneur_individuel ? rne_infos.dirigeants[0].prenoms.toUpperCase() + ' ' + rne_infos.dirigeants[0].nom.toUpperCase() : rne_infos.nom_complet.toUpperCase(),
					address: (rne_infos.siege.numero_voie ? rne_infos.siege.numero_voie : '') + (rne_infos.siege.indice_repetition ? rne_infos.siege.indice_repetition : '') + (rne_infos.siege.type_voie ? ' ' + rne_infos.siege.type_voie : '') + (rne_infos.siege.libelle_voie ? ' ' + rne_infos.siege.libelle_voie : '') + (rne_infos.siege.complementLocalisation ? ' ' + rne_infos.siege.complemen_adresse : ''),
					type: rne_infos.nature_juridique,
					zipcode: rne_infos.siege.code_postal,
					city: rne_infos.siege.commune,
					city_name: rne_infos.siege.libelle_commune.toUpperCase()
				})
				.then(response =>
				{
					if (response.code == 200)
					{
						document.querySelector('.title').innerText = response.data.displayname
						document.getElementById('name').value = response.data.displayname
						document.getElementById('type').value = response.data.type
						document.getElementById('address').value = response.data.address
						document.getElementById('zipcode').value = response.data.zipcode
						get_cities(document.getElementById('city'), response.data.zipcode, response.data.city, false)
					}
				})
		}

		function change_country(save = true)
		{
			if (document.getElementById('country').value == 74)
			{
				document.getElementById('city_name_container').style.display = 'none'
				document.getElementById('city_container').style.display = ''
				if (save)
					get_cities(document.getElementById('city'), document.getElementById('zipcode').value, document.getElementById('city').value, save)
			}
			else
			{
				document.getElementById('city_name_container').style.display = ''
				document.getElementById('city_container').style.display = 'none'
			}
			return true
		}
	}
}