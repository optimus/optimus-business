export default class OptimusBusinessEditorBankAccount
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		if (admin_only()) return

		await load('/services/optimus-business/businesses/accounting.html', this.target, null, true)

		loader(this.target, true)
		const inputCollection = document.querySelectorAll("input,select,textarea")
		for (const el of inputCollection)
		{
			el.setAttribute('data-endpoint', '{server}/optimus-business/businesses/{id}')
			el.setAttribute('data-field', el.id)
		}
		this.response = await rest('https://' + this.server + '/optimus-business/businesses/' + this.id)

		await populate(document.getElementById('accounting_method'), store.user.server + '/optimus-business/constants/accounting_methods', true, false)
		await populate(document.getElementById('vat_scheme'), store.user.server + '/optimus-business/constants/vat_schemes', true, false)
		await populate(document.getElementById('invoices_numbering_format'), store.user.server + '/optimus-business/constants/invoices_numbering_formats', true, false)

		let optimusForm = await load('/components/optimus_form.js', this.target, this.response.data)

		loader(this.target, false)
	}
}
