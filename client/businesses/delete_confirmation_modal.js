export default class OptimusBusinessBusinessesDeleteConfirmationModal
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.id = store.queryParams.id
	}

	async init()
	{
		if (admin_only()) return

		await load('/services/optimus-business/businesses/delete_confirmation_modal.html', this.target)

		modal.querySelector('.delete-button').onclick = () =>
		{
			rest(this.server + '/optimus-business/businesses/' + this.id, 'DELETE', null, 'toast')
				.then(response => 
				{
					if (response.code == 200)
					{
						router('optimus-business/businesses')
						modal.close()
					}
				})
		}

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}
}