export default class OptimusBusinessBusinessesFiles
{
	constructor(target, params)
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.id = store.queryParams.id
	}

	async init()
	{
		let owner = await rest(this.server + '/optimus-base/users/' + this.id)

		load('/components/optimus_webdav_explorer/index.js', this.target,
			{
				id: 'files',
				title: 'Fichiers',
				resource: 'files',
				server: 'https://' + store.user.server.replace('api.', 'cloud.'),
				root: 'files',
				path: '/' + owner.data.email,
				cannotGetHigherThan: '/' + owner.data.email,
				showBreadcrumb: false,
				showSearchBox: false,
				showRecursiveSearchButton: false,
			}
		)
	}
}