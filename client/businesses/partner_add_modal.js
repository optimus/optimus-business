export default class OptimusBusinessEditorPartnerAddModal
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.id = store.queryParams.id
	}

	async init()
	{
		if (admin_only()) return

		await load('/services/optimus-business/businesses/partner_add_modal.html', this.target)

		setTimeout(() => modal.querySelector('.users').focus(), 10)

		modal.querySelector('.server').onchange = async selectedValue => 
		{
			let partners = await rest('https://api.' + modal.querySelector('.server').value + '/optimus-business/' + this.id + '/partners')
			let partners_ids = partners.data.filter(partner => partner.server == modal.querySelector('.server').value).map(partner => partner.user)

			rest('https://api.' + modal.querySelector('.server').value + '/optimus-base/users')
				.then(response => 
				{
					clear(modal.querySelector('.users'))
					for (let user of response.data)
						if (!partners_ids.includes(user.id))
							modal.querySelector('.users').add(new Option(user.displayname, user.id))
					if (selectedValue)
						modal.querySelector('.users').value = selectedValue
				})
		}

		modal.querySelector('.fa-user-edit').onclick = () => 
		{
			if (modal.querySelector('.users').options.length == 0)
				return false
			modal.hide()
			router('optimus-base/users/editor?id=' + modal.querySelector('.users').value + '#general')
		}

		modal.querySelector('.fa-user-plus').onclick = () => 
		{
			rest('https://api.' + modal.querySelector('.server').value + '/optimus-base/users', 'POST')
				.then(response => 
				{
					if (response.code == 201)
					{
						modal.querySelector('.users').add(new Option(response.data.displayname, response.data.id))
						modal.querySelector('.users').value = response.data.id
						modal.hide()
						router('optimus-base/users/editor?id=' + modal.querySelector('.users').value + '#general')
					}
				})
		}

		modal.onshow = () => modal.querySelector('.server').onchange(modal.querySelector('.users').value)

		modal.querySelector('.arrival').valueAsDate = new Date()
		modal.querySelector('.server').value = store.user.server.replace('api.', '')
		modal.querySelector('.server').onchange()

		modal.querySelector('.add-button').onclick = () =>
		{
			let data =
			{
				server: modal.querySelector('.server').value,
				user: modal.querySelector('.users').value,
				arrival: modal.querySelector('.arrival').value,
				departure: modal.querySelector('.departure').value || null
			}
			rest(this.server + '/optimus-business/' + this.id + '/partners', 'POST', data, 'toast')
				.then(response => 
				{
					if (response.code == 201)
					{
						this.params.add_partner(response.data, true)
						modal.close()
					}
				})
		}

		modal.querySelector('.arrival-datepicker').onclick = () => load('/components/optimus_datepicker.js', modal, { input: modal.querySelector('.arrival') })
		modal.querySelector('.departure-datepicker').onclick = () => load('/components/optimus_datepicker.js', modal, { input: modal.querySelector('.departure') })

		modal.querySelector('.modal-card-close').onclick = () => modal.close()
		modal.querySelector('.cancel-button').onclick = () => modal.close()
	}
}