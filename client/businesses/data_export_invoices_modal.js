export default class OptimusBusinessEditorPartnerAddModal
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.id = store.queryParams.id
	}

	async init()
	{
		await load('/services/optimus-business/businesses/data_export_invoices_modal.html', this.target)

		this.websocketIncomingListener = message => 
		{
			if (message.detail.command == "optimus-business_export_invoices")
			{
				modal.querySelector('.export-progress').value = message.detail.data.value
				modal.querySelector('.export-progress').max = message.detail.data.max
				modal.querySelector('.value').innerText = message.detail.data.value
				modal.querySelector('.max').innerText = message.detail.data.max

				if (message.detail.data.value == message.detail.data.max)
					modal.querySelector('.modal-card-foot').classList.remove('is-hidden')
			}
		}
		optimusWebsocket.addEventListener('incoming', this.websocketIncomingListener)

		rest(store.user.server + '/optimus-business/' + this.id + '/invoices_zip', 'GET', this.params)
			.then(response =>
			{
				if (response.code == 200)
				{
					modal.querySelector('.download-button').classList.remove('is-loading')
					modal.querySelector('.modal-card-close').onclick = () => modal.close()
					modal.querySelector('.download-button').onclick = () =>
					{
						var element = document.createElement('a')
						element.href = 'data:application/octet-stream;base64,' + response.data
						element.download = 'Factures du ' + this.params.start_date + ' au ' + this.params.end_date + '.zip'
						element.click()
						element.remove()
					}
				}
			})
	}

	onUnload()
	{
		optimusWebsocket.removeEventListener('incoming', this.websocketIncomingListener)
		return this
	}
}
