export default class OptimusBusinessBusinesses
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
	}

	init()
	{
		if (admin_only()) return

		let table = load('/components/optimus_table/index.js', this.target,
			{
				id: 'businesses',
				title: 'Entreprises',
				version: 2,
				resource: 'businesses',
				addIcon: 'fa-building',
				url: 'https://' + store.user.server + '/optimus-business/businesses',
				striped: true,
				bordered: true,
				columnSeparator: true,
				fullscreen: true,
				tabulator:
				{
					progressiveLoad: 'auto',
					columns:
						[
							{ field: "id", title: 'ID', sorter: 'number', sorterParams: { type: 'integer' } },
							{ field: 'displayname', title: 'Nom', width: 350 },
							{ field: 'type_description', title: 'Forme', visible: false },
							{ field: 'siren', title: 'SIREN', hozAlign: 'center' },
							{ field: 'address', title: 'Adresse' },
							{ field: 'zipcode', title: 'CP', sorter: 'number' },
							{ field: 'city_name', title: 'Ville' },
							{ field: 'pays_description', title: 'Pays', width: 150 },
							{ field: 'phone', title: 'Téléphone' },
							{ field: 'fax', title: 'Fax', visible: false },
							{ field: 'mobile', title: 'Portable' },
							{ field: 'email', title: 'Email', width: 150 },
							{ field: 'website', title: 'Site', visible: false },
						],
				},
				rowClick: (event, row) => router('optimus-business/businesses/editor?id=' + row.getData().id + '#general'),
				addClick: () =>
					rest(store.user.server + '/optimus-business/businesses', 'POST', {}, 'toast')
						.then(data => data.code == 201 && router('optimus-business/businesses/editor?id=' + data.data.id + '#general'))
			})
	}
}