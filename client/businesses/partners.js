export default class OptimusBusinessEditorPartners
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.id = store.queryParams.id
	}

	async init()
	{
		if (admin_only()) return

		await load('/services/optimus-business/businesses/partners.html', this.target, null, true)
		this.target.querySelectorAll('.box').forEach(object => loader(object, true, 'list'))

		rest('https://' + this.server + '/optimus-business/' + this.id + '/partners')
			.then(response => 
			{
				for (let partner of response.data)
					this.add_partner(partner)
			})

		main.querySelector('.partner-add-button').onclick = () => modal.open('/services/optimus-business/businesses/partner_add_modal.js', false, { add_partner: this.add_partner })

		this.target.querySelectorAll('.box').forEach(object => loader(object, false))
	}

	async add_partner(partner, animation)
	{
		const template = document.getElementById('partner').content.firstElementChild.cloneNode(true)

		for (const element of template.querySelectorAll("input,select,textarea"))
			element.setAttribute('data-endpoint', this.server + '/optimus-business/' + this.id + '/partners/' + partner.id)
		await load('/components/optimus_form.js', template, partner)

		template.querySelector('[data-field="displayname"]').onmouseover = function () { this.classList.add('is-underlined') }
		template.querySelector('[data-field="displayname"]').onmouseleave = function () { this.classList.remove('is-underlined') }
		template.querySelector('[data-field="displayname"]').onclick = () => router('optimus-base/users/editor?id=' + partner.user + '#general')

		template.querySelector('.partner-delete-button').onmouseover = function () { template.querySelector('.message-header').classList.add('has-background-danger') }
		template.querySelector('.partner-delete-button').onmouseleave = function () { template.querySelector('.message-header').classList.remove('has-background-danger') }

		template.querySelector('.partner-delete-button').onclick = () => modal.open('/services/optimus-business/businesses/partner_delete_confirmation_modal.js', true, { template: template, entreprise_id: store.queryParams.id, partner_id: partner.id })

		if (animation)
			template.classList.add('animate__animated', 'animate__zoomIn', 'animate__faster')

		main.querySelector('.partners').appendChild(template)
	}
}
