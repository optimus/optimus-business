export default class OptimusBusinessEditorBankAccount
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.id = store.queryParams.id
	}

	async init()
	{
		if (admin_only()) return

		await load('/services/optimus-business/businesses/bankaccounts.html', this.target, null, true)
		//this.target.querySelectorAll('.box').forEach(object => loader(object, true, 'list'))
		//this.target.querySelectorAll('.box').forEach(object => loader(object, false))
	}
}
