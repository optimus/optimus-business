load_CSS('/libs/tabulator/css/tabulator_bulma.min.css')
load_script('/libs/luxon.min.js')
import { Tabulator, ColumnCalcsModule, FormatModule, InteractionModule, SortModule, AjaxModule } from '/libs/tabulator/js/tabulator_esm.min.js'
Tabulator.registerModule([ColumnCalcsModule, FormatModule, InteractionModule, SortModule, AjaxModule])

export default class OptimusBusinessInvoiceTab
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.initial_filters = params.initial_filters
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
		return this
	}

	async init()
	{
		if (!store.queryParams.id)
			return this.target.innerHTML = "Aucun identifiant (id) n'a été renseigné"

		loader(this.target, true)
		await load('/services/optimus-business/invoices_tab/index.html', this.target)

		this.businesses = await rest('https://' + store.user.server + '/optimus-business/users/' + store.user.id + '/businesses')
			.then(response => 
			{
				if (response.code != 200)
					return optimusToast("Vous n'êtes associé d'aucune entreprise", "is-warning")
				else
					return response.data
			})

		this.target.querySelector('.show-paid').onclick = () => this.tabulator.setData()
		this.target.querySelector('.show-unpaid').onclick = () => this.tabulator.setData()

		this.tabulator = new Tabulator(this.target.querySelector('.optimus-table-container'),
			{
				ajaxURL: 'unused',
				ajaxRequestFunc: () => new Promise((resolve, reject) =>
				{
					this.endpoints = []
					this.columns = ['id', 'displayname', 'date', 'total_tax_exclusive', 'total_vat', 'total_tax_inclusive', 'total_paid', 'total_unpaid', 'percent_paid']

					for (let business of this.businesses)
						this.endpoints.push(rest('https://api.' + business.server + '/optimus-business/' + business.id + '/invoices', 'GET', { columns: this.columns, filter: this.getFilters() }))

					Promise.all(this.endpoints)
						.then(responses => 
						{
							for (let response of responses)
								if (response.code != 200 && response.code != 404)
									reject(optimusToast(response.message, 'is-danger'))
							let data = []
							for (let i = 0; i < this.businesses.length; i++)
								for (let j = 0; j < responses[i].data.length; j++)
								{
									responses[i].data[j].business_server = this.businesses[i].server
									responses[i].data[j].business_id = this.businesses[i].id
									responses[i].data[j].business_displayname = this.businesses[i].displayname
								}
							for (let response of responses)
								data = data.concat(response.data)
							resolve(data)
						})
				}),
				ajaxConfig: {
					method: 'GET',
					credentials: 'include'
				},
				initialSort: [{ column: 'date', dir: 'desc' }],
				dataLoader: false,
				ajaxContentType: 'json',
				layout: 'fitColumns',
				layoutColumnsOnNewData: false,
				sortOrderReverse: true,
				headerSortElement: "<i class='fas fa-arrow-up'></i>",
				locale: 'fr',
				placeholder: 'Aucun résultat',
				langs: {
					'fr': {
						ajax: {
							loading: "Chargement",
							error: "Erreur de chargement",
						},
					}
				},
				columns:
					[
						{
							field: 'pdf',
							title: 'Pdf',
							formatter: cell => '<i class="fas fa-file-pdf"></i>',
							titleFormatter: cell => '<i class="fas fa-file-pdf"></i>',
							hozAlign: 'center',
							headerHozAlign: 'center',
							headerSort: false,
							minWidth: 44,
							maxWidth: 44,
							cellMouseOver: (event, cell) => cell.getElement().querySelector('i')?.classList.add('has-text-link'),
							cellMouseOut: (event, cell) => cell.getElement().querySelector('i')?.classList.remove('has-text-link'),
							cellClick: (event, cell) => 
							{
								if (cell.getElement().parentNode.classList.contains('tabulator-calcs'))
									return false
								cell.getElement().querySelector('i').classList.replace('fa-file-pdf', 'fa-spinner')
								cell.getElement().querySelector('i').classList.add('fa-spin')
								rest('https://api.' + cell.getData().business_server + '/optimus-business/' + cell.getData().business_id + '/invoices/' + cell.getData().id + '/pdf')
									.then(file => 
									{
										if (file.code == 200)
										{
											var element = document.createElement('a')
											element.href = 'data:application/octet-stream;base64,' + file.data
											element.download = cell.getData().displayname + '.pdf'
											element.click()
											element.remove()
										}
										cell.getElement().querySelector('i').classList.replace('fa-spinner', 'fa-file-pdf')
										cell.getElement().querySelector('i').classList.remove('fa-spin')
									})
							}
						},
						{
							field: 'view_pdf',
							title: 'Pdf',
							formatter: cell => '<i class="fas fa-eye"></i>',
							titleFormatter: cell => '<i class="fas fa-eye"></i>',
							hozAlign: 'center',
							headerHozAlign: 'center',
							headerSort: false,
							minWidth: 44,
							maxWidth: 44,
							cellMouseOver: (event, cell) => cell.getElement().querySelector('i')?.classList.add('has-text-link'),
							cellMouseOut: (event, cell) => cell.getElement().querySelector('i')?.classList.remove('has-text-link'),
							cellClick: (event, cell) => 
							{
								if (cell.getElement().parentNode.classList.contains('tabulator-calcs'))
									return false
								if (store.navigator == "Safari")
									return window.open('https://' + store.user.server + '/optimus-business/' + store.queryParams.owner + '/invoices/' + cell.getData().id + '/pdf?mode=download')
								cell.getElement().querySelector('i').classList.replace('fa-eye', 'fa-spinner')
								cell.getElement().querySelector('i').classList.add('fa-spin')
								rest('https://api.' + cell.getData().business_server + '/optimus-business/' + cell.getData().business_id + '/invoices/' + cell.getData().id + '/pdf')
									.then(file => 
									{
										if (file.code == 200)
										{
											let byteCharacters = atob(file.data)
											let byteNumbers = new Array(byteCharacters.length)
											for (let i = 0; i < byteCharacters.length; i++)
												byteNumbers[i] = byteCharacters.charCodeAt(i)
											let byteArray = new Uint8Array(byteNumbers)
											let pdfFile = new Blob([byteArray], { type: 'application/pdf;base64' })
											let fileURL = URL.createObjectURL(pdfFile)
											window.open(fileURL)
										}
										cell.getElement().querySelector('i').classList.replace('fa-spinner', 'fa-eye')
										cell.getElement().querySelector('i').classList.remove('fa-spin')
									})
							}
						},
						{
							field: 'business_displayname',
							title: 'Entreprise',
							minWidth: 150,
							width: 350
						},
						{
							field: 'displayname',
							title: 'Numéro',
							minWidth: 120,
							maxWidth: 120
						},
						{
							field: 'date',
							title: 'Date',
							minWidth: 120,
							maxWidth: 120,
							formatter: cell => cell.getValue() ? new Date(cell.getValue()).toLocaleDateString('fr') : null,
							hozAlign: 'center',
						},
						{
							field: 'total_tax_exclusive',
							title: 'Total HT',
							formatter: "money",
							formatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
							hozAlign: 'right',
							bottomCalc: 'sum',
							bottomCalcFormatter: (cell, params, callback) =>
							{
								let money = cell.getTable().modules.format.constructor.formatters.money
								cell.getElement().classList.add('has-text-weight-normal', 'pr-4')
								return money(cell, params, callback)
							},
							bottomCalcFormatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
							minWidth: 120
						},
						{
							field: 'total_vat',
							title: 'Total TVA',
							formatter: "money",
							formatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
							hozAlign: 'right',
							bottomCalc: 'sum',
							bottomCalcFormatter: (cell, params, callback) =>
							{
								let money = cell.getTable().modules.format.constructor.formatters.money
								cell.getElement().classList.add('has-text-weight-normal', 'pr-4')
								return money(cell, params, callback)
							},
							bottomCalcFormatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
							minWidth: 120,
						},
						{
							field: 'total_tax_inclusive',
							title: 'Total TTC',
							formatter: "money",
							formatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
							hozAlign: 'right',
							bottomCalc: 'sum',
							bottomCalcFormatter: (cell, params, callback) =>
							{
								let money = cell.getTable().modules.format.constructor.formatters.money
								cell.getElement().classList.add('has-text-weight-normal', 'pr-4')
								return money(cell, params, callback)
							},
							bottomCalcFormatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
							minWidth: 120,
						},
						{
							field: 'total_paid',
							title: 'Payé',
							formatter: (cell, params, callback) =>
							{
								let money = cell.getTable().modules.format.constructor.formatters.money
								cell.getElement().classList.add('has-text-success')
								return money(cell, params, callback)
							},
							formatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
							hozAlign: 'right',
							bottomCalc: 'sum',
							bottomCalcFormatter: (cell, params, callback) =>
							{
								let money = cell.getTable().modules.format.constructor.formatters.money
								cell.getElement().classList.add('has-text-success', 'has-text-weight-normal', 'pr-4')
								return money(cell, params, callback)
							},
							bottomCalcFormatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
							minWidth: 120
						},
						{
							field: 'total_unpaid',
							title: 'Impayé',
							formatter: (cell, params, callback) =>
							{
								let money = cell.getTable().modules.format.constructor.formatters.money
								cell.getElement().classList.add('has-text-danger')
								return money(cell, params, callback)
							},
							formatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
							hozAlign: 'right',
							bottomCalc: 'sum',
							bottomCalcFormatter: (cell, params, callback) =>
							{
								let money = cell.getTable().modules.format.constructor.formatters.money
								cell.getElement().classList.add('has-text-danger', 'has-text-weight-normal', 'pr-4')
								return money(cell, params, callback)
							},
							bottomCalcFormatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
							minWidth: 120
						},
						{
							field: 'percent_paid',
							title: '%',
							formatter: cell => 
							{
								if (cell.getValue() >= 100)
									return '<span class="has-text-success">' + Math.round(cell.getValue(), 2) + ' %</span>'
								return '<span class="has-text-danger">' + Math.round(cell.getValue(), 2) + ' %</span>'
							},
							hozAlign: 'right',
							minWidth: 70,
						},
					]
			})

		this.tabulator.on("dataLoadError", () => loader(this.target, false))

		this.tabulator.on("dataLoaded", () => loader(this.target, false))

		this.tabulator.on("tableBuilt", () => this.target?.querySelector('.tabulator-footer-contents')?.remove())

		this.tabulator.on("rowClick", (event, row) => 
		{
			if (event.target.parentNode.classList.contains('tabulator-calcs') || event.target.getAttribute('tabulator-field') == 'pdf' || event.target.classList.contains('fa-file-pdf') || event.target.getAttribute('tabulator-field') == 'view-pdf' || event.target.classList.contains('fa-eye'))
				return false
			router('optimus-business/invoices/editor?owner=' + row.getData().business_id + '&id=' + row.getData().id + '#general')
		})

		this.tabulator.on("rowMouseEnter", (event, row) => !event.target.parentNode.classList.contains('tabulator-calcs') && row.getElement().classList.add('is-clickable', 'has-text-link'))

		this.tabulator.on("rowMouseLeave", (event, row) => !event.target.parentNode.classList.contains('tabulator-calcs') && row.getElement().classList?.remove('has-text-link'))
	}

	getFilters()
	{
		this.filters = []
		this.filters = this.filters.concat(this.initial_filters)

		if (!this.target.querySelector('.show-paid').checked)
			this.filters.push({ field: 'percent_paid', type: '<', value: 100 })

		if (!this.target.querySelector('.show-unpaid').checked)
			this.filters.push({ field: 'percent_paid', type: '>=', value: 100 })

		return this.filters
	}

	async getData()
	{
		let data = []
		let businesses = [66, 67]
		for (let business of businesses)
		{
			let invoices = await rest('https://' + store.user.server + '/optimus-business/' + business + '/invoices', 'GET', this.filters)
			data.concat(invoices)
		}
		return data
	}

	onWindowResize()
	{
		this.tabulator.redraw()
	}
}