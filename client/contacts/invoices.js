export default class OptimusAvocatsDossiersFactures
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
		return this
	}

	init()
	{
		load('/services/optimus-business/invoices_tab/index.js', this.target,
			{
				initial_filters:
					[
						{ field: 'client_endpoint', type: '=', value: '/optimus-contacts/{owner}/contacts/{id}' },
						{ field: 'client_server', type: '=', value: store.user.server.replace('api.', '') },
						{ field: 'client_owner', type: '=', value: store.queryParams.owner },
						{ field: 'client_id', type: '=', value: store.queryParams.id },
					]
			})
	}
}