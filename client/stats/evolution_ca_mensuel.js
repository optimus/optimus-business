export default class OptimusStructuresEvolutionCaMensuel
{
	constructor(target)
	{
		this.target = target
	}

	init()
	{
		clear(this.target)
		return load('/components/optimus_chart.js', this.target,
			{
				type: 'line',

				data:
				{
					labels: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
					datasets:
						[
							{
								data: [40000, 50000, 70000, 45000, 50000, 55000, 35000, 25000, 50000, 60000, 70000, 100000],
								fill: false,
								tension: 0.1,
								pointStyle: 'circle',
								pointRadius: this.target.offsetWidth / 200,
								pointHoverRadius: this.target.offsetWidth / 150,
							}
						]
				},

				options:
				{
					aspectRatio: 2,
					maintainAspectRatio: true,
					animation:
					{
						duration: 500,
						easing: 'easeOutSine'
					},
					scales:
					{
						y:
						{
							ticks:
							{
								callback: function (value)
								{
									return value + ' €'
								},
								font:
								{
									size: Math.max(this.target.offsetWidth / 100, 12)
								}
							}
						},
						x:
						{
							ticks:
							{
								font:
								{
									size: Math.max(this.target.offsetWidth / 100, 12)
								}
							},
						},
					},
					plugins:
					{
						legend:
						{
							display: false
						},
						title:
						{
							display: true,
							text: "EVOLUTION DU CHIFFRE D'AFFAIRES",
							padding:
							{
								top: 10,
								bottom: 30
							},
							font:
							{
								size: Math.max(this.target.offsetWidth / 60, 12)
							}
						}
					},
				}
			}
		)
	}
}
