export default class OptimusStructuresJaugeObjectif
{
	constructor(target)
	{
		this.target = target
	}

	init()
	{
		clear(this.target)
		return load('/components/optimus_chart.js', this.target,
			{
				type: 'pie',
				data: {
					datasets: [{
						data: [25, 25, 50],
						objectif: 10000,
						valeur: 15000,
						backgroundColor: [
							'rgba(255, 99, 132)',
							'rgba(255, 206, 86)',
							'rgba(0, 209, 178)'
						],
						borderWidth: 0,
					}]
				},

				options:
				{
					aspectRatio: 2,
					maintainAspectRatio: true,
					rotation: -90,
					circumference: 180,
					cutout: '70%',
					animation:
					{
						duration: 500,
						easing: 'easeOutSine',
						onComplete: function (animation)
						{
							var chart = animation.chart
							var ctx = animation.chart.canvas.getContext('2d')
							var radianAngle = (180 - (180 * Math.min(parseFloat(chart.getDatasetMeta(0)._dataset?.valeur) / 2 / parseFloat(chart.getDatasetMeta(0)._dataset?.objectif), 1))) * Math.PI / 180
							//AIGUILLE
							ctx.translate(chart.chartArea.width / 2, chart.chartArea.bottom)
							ctx.rotate(-radianAngle)
							ctx.beginPath()
							ctx.moveTo(0, -chart.chartArea.width / 60)
							ctx.lineTo(chart.chartArea.height / 1.5, 0)
							ctx.lineTo(0, chart.chartArea.width / 60)
							ctx.fillStyle = Chart.defaults.color
							ctx.fill()
							//CERCLE
							ctx.rotate(radianAngle)
							ctx.translate(-chart.chartArea.width / 2, -chart.chartArea.bottom)
							ctx.beginPath()
							ctx.arc(chart.chartArea.width / 2, chart.chartArea.bottom, chart.chartArea.width / 60, 0, Math.PI * 2)
							ctx.fillStyle = Chart.defaults.color
							ctx.fill()

							ctx.textAlign = 'center'
							ctx.fillStyle = Chart.defaults.color
							ctx.font = 'bold ' + Math.max(chart.canvas.offsetHeight / 30, 12) + 'px Helvetica'
							ctx.fillText('Encaissé : ' + Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR', maximumFractionDigits: 0 }).format(chart.getDatasetMeta(0)._dataset?.valeur), chart.chartArea.width / 2, chart.chartArea.bottom + 3 * Math.max(chart.canvas.offsetHeight / 30, 12))
						}
					},
					plugins:
					{
						title:
						{
							display: true,
							text: ["OBJECTIF MENSUEL", Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR', maximumFractionDigits: 0 }).format(10000)],
							padding: { top: 10, bottom: this.target.offsetHeight / 50 },
							font:
							{
								size: Math.max(this.target.offsetHeight / 40, 12)
							}
						}
					},
					interaction: { mode: null },
					layout: { padding: { bottom: 3 * Math.max(this.target.offsetHeight / 30, 12) + 10 } }
				}
			})
			.then(component => this.chart = component.chart)
	}
}