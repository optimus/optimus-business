export default class OptimusStructuresTop5CaMensuel
{
	constructor(target)
	{
		this.target = target
	}

	init()
	{

		clear(this.target)
		return load('/components/optimus_chart.js', this.target,
			{
				type: 'bar',

				data:
				{
					labels:
						[
							'Associé 1',
							'Associé 2',
							'Collaborateur 1',
							'Collaborateur 2',
							'Collaborateur 3'
						],
					datasets:
						[
							{
								label: 'C.A. du mois',
								data: [10000, 9500, 8550, 7000, 2000],
								backgroundColor: [
									'rgb(255, 99, 132)',
									'rgb(54, 162, 235)',
									'rgb(255, 205, 86)'
								],
								hoverOffset: 4
							}
						]
				},

				options:
				{
					aspectRatio: 2,
					maintainAspectRatio: true,
					animation:
					{
						duration: 500,
						easing: 'easeOutSine'
					},
					scales:
					{
						y:
						{
							ticks:
							{
								callback: function (value)
								{
									return value + ' €'
								},
								font:
								{
									size: Math.max(this.target.offsetWidth / 100, 12)
								}
							}
						},
						x:
						{
							ticks:
							{
								font:
								{
									size: Math.max(this.target.offsetWidth / 100, 12)
								}
							}
						}
					},
					plugins:
					{
						legend:
						{
							display: false
						},
						title:
						{
							display: true,
							text: "TOP 5 CHIFFRE D'AFFAIRES MENSUEL",
							padding:
							{
								top: 10,
								bottom: 30
							},
							font:
							{
								size: Math.max(this.target.offsetWidth / 60, 12)
							}
						}
					}
				}
			})
	}
}