export default class OptimusBusinessService
{
	constructor()
	{
		this.name = 'optimus-business'
		this.resources = [
			{
				id: 'invoices',
				name: 'invoices',
				description: 'Toutes les factures',
				path: 'invoices'
			},
			{
				id: 'invoice',
				name: 'invoice',
				description: 'Une facture déterminée',
				path: 'invoices/id',
				endpoint: '/optimus-businesses/{owner}/invoices'
			}
		]
		this.swagger_modules =
			[
				{
					id: "optimus-business",
					title: "OPTIMUS BUSINESS",
					path: "/services/optimus-business/optimus-business.yaml",
					filters: ["accounts", "businesses", "constants", "invoices", "nordigen", "payments", "partners", "reminders", "transactions", "service"]
				},
			]
		this.optimus_contacts_editor_tabs =
			[
				{
					id: "tab_invoices",
					text: "Factures",
					link: "/services/optimus-business/contacts/invoices.js",
					position: 650
				}
			]
		this.optimus_avocats_dossiers_tabs =
			[
				{
					id: "tab_invoices",
					text: "Factures",
					link: "/services/optimus-business/dossiers/invoices.js",
					position: 650
				}
			]
		store.services.push(this)
	}

	login()
	{
		leftmenu.create('optimus-business', 'BUSINESS')
		leftmenu['optimus-business'].add('businesses', 'Entreprises', 'fas fa-building', () => router('optimus-business/businesses'))
		leftmenu['optimus-business'].add('invoices', 'Factures', 'fas fa-file-invoice-dollar', () => router('optimus-business/invoices'))

		store.resources = store.resources.concat(this.resources)
	}

	logout()
	{
		leftmenu['optimus-business'].remove()

		for (let resource of this.resources)
			store.resources.splice(store.resources.findIndex(item => item.id == resource.id), 1)

		store.services = store.services.filter(service => service.name != this.id)
	}

	async global_search(search_query)
	{
		let authorizations = await rest('https://' + store.user.server + '/optimus-base/authorizations', 'GET', { user: store.user.id, resource: 'invoices' })
		return [
			{
				icon: 'fas fa-file-invoice-dollar',
				title: 'Factures',
				data: rest(store.user.server + '/optimus-business/' + authorizations.data[0].owner + '/invoices', 'GET', { filter: [{ field: "*", type: "like", value: search_query }], references: true, size: 12, page: 1 })
					.then(contacts => contacts?.data?.map(invoice => { return { displayname: invoice.displayname + ' (' + new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(invoice.total_tax_inclusive) + ')', route: 'optimus-business/invoices/editor?owner=' + authorizations.data[0].owner + '&id=' + invoice.id + '#general' } }))
			}
		]
	}

	dashboard()
	{
		// quickactions_add('quickaction-structures-header', 'header', 'CABINET')
		// quickactions_add('quickaction-structures-facture-create', 'item', 'Créer une facture', 'fas fa-file-invoice-dollar', () => quickactionCabinetFactureCreate())
		// fetch('/services/optimus-structures/dashboard.html').then(response => response.text()).then(response => document.getElementById('dashboard').insertAdjacentHTML('beforeend', response)).then(() => 
		// {
		// 	load('/services/optimus-structures/stats/top5_ca_mensuel.js', document.getElementById('top5-ca-mensuel'))
		// 	load('/services/optimus-structures/stats/evolution_ca_mensuel.js', document.getElementById('evolution-ca-mensuel'))
		// 	load('/services/optimus-structures/stats/jauge_objectif.js', document.getElementById('jauge-objectif'))
		// })
	}


}
