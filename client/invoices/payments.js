load_CSS('/libs/tabulator/css/tabulator_bulma.min.css')
load_script('/libs/luxon.min.js')
import { Tabulator, EditModule, FormatModule, InteractionModule, KeybindingsModule, MutatorModule, SortModule, AjaxModule } from '/libs/tabulator/js/tabulator_esm.min.js'
Tabulator.registerModule([EditModule, FormatModule, InteractionModule, KeybindingsModule, MutatorModule, SortModule, AjaxModule])
export default class OptimusInvoiceEditorPayments
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		if (!store.queryParams.id)
			return this.target.innerHTML = "Aucun identifiant (id) n'a été renseigné"

		loader(this.target, true)
		await load('/services/optimus-business/invoices/payments.html', this.target)

		this.update()

		let datepicker = await load('/components/optimus_table/datepicker.js')

		this.target.querySelector('.add-button').onclick = () =>
			rest('https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id + '/payments', 'POST', { invoice: this.id })
				.then(response => 
				{
					if (response.code == 201)
						this.tabulator.addRow(response.data).then(row => 
						{
							this.tabulator.setSort(this.tabulator.getSorters())
							row.getCell('amount').edit()
							this.update()
						})
				})

		this.tabulator = new Tabulator(this.target.querySelector('.optimus-table-container'),
			{
				ajaxURL: 'https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id + '/payments',
				ajaxRequestFunc: (url, ajaxConfig, params) => new Promise(function (resolve, reject)
				{
					if (!store.user.server)
						modal.open('/services/optimus-base/login/index.js')
					else
						rest(url, ajaxConfig.method, params, 'toast')
							.then(response =>
							{
								if (response.code >= 400)
									reject(response.message)
								else
									resolve(response.data)
							})
				}),
				ajaxConfig: {
					method: 'GET',
					credentials: 'include'
				},
				initialSort: [{ column: 'date', dir: 'asc' }],
				dataLoader: false,
				ajaxContentType: 'json',
				layout: 'fitColumns',
				layoutColumnsOnNewData: false,
				sortOrderReverse: true,
				headerSortElement: "<i class='fas fa-arrow-down'></i>",
				locale: 'fr',
				langs: {
					'fr': {
						ajax: {
							loading: "Chargement",
							error: "Erreur de chargement",
						},
					}
				},
				columns:
					[
						{
							field: 'date',
							title: 'Date',
							hozAlign: 'center',
							minWidth: 120,
							maxWidth: 120,
							formatter: 'datetime',
							formatterParams:
							{
								inputFormat: "yyyy-MM-dd",
								outputFormat: "dd/MM/yyyy",
								invalidPlaceholder: '<span class="has-text-danger">(date invalide)</span>',
							},
							editor: datepicker.open,
							editorParams:
							{
								optimusDatepicker:
								{
									type: 'date',
									showClearButton: true,
									closeOnOverlayClick: true,
									required: true
								}
							},
							cellEdited: cell => rest('https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id + '/payments/' + cell.getData().id, 'PATCH', { date: cell.getValue() }).then(response => this.tabulator.setSort(this.tabulator.getSorters()))
						},
						{
							field: 'amount',
							title: 'Montant',
							editor: 'number',
							hozAlign: 'right',
							minWidth: 120,
							maxWidth: 120,
							editorParams:
							{
								min: 0.01,
								step: 0.01,
								selectContents: true,
							},
							formatter: "money",
							formatterParams:
							{
								decimal: ",",
								thousand: " ",
								symbol: " €",
								symbolAfter: true,
								negativeSign: true,
								precision: 2,
							},
							cellEdited: cell => 
							{
								return rest('https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id + '/payments/' + cell.getData().id, 'PATCH', { amount: cell.getValue() })
									.then(response => 
									{
										if (response.code == 200)
										{
											this.tabulator.setSort(this.tabulator.getSorters())
											this.update()
											return true
										}
										else
										{
											cell.restoreOldValue()
											return false
										}
									})
							}
						},
						{
							field: 'delete',
							title: 'delete',
							hozAlign: 'center',
							minWidth: 44,
							maxWidth: 44,
							titleFormatter: () => '',
							formatter: () => '<i class="fas fa-trash"></i>',
							cellMouseOver: (event, cell) => 
							{
								cell.getElement().classList.add('is-clickable')
								cell.getElement().querySelector('i').classList.add('has-text-danger')
							},
							cellMouseOut: (event, cell) => cell.getElement().querySelector('i').classList.remove('has-text-danger'),
							cellClick: (event, cell) => rest('https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id + '/payments/' + cell.getData().id, 'DELETE')
								.then(response => 
								{
									if (response.code == 200)
									{
										cell.getRow().delete()
										this.update()
									}
								})
						},
					]
			})

		this.tabulator.on("dataLoadError", () => loader(this.target, false))
		this.tabulator.on("dataProcessed", () => 
		{
			setTimeout(() => this.tabulator.element.classList.toggle('is-hidden', this.tabulator.getData().length == 0), 1)
			loader(this.target, false)
		})
		this.tabulator.on("dataChanged", data => this.tabulator.element.classList.toggle('is-hidden', data.length == 0))
	}

	update()
	{
		return rest('https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id)
			.then(invoice =>
			{
				this.target.querySelector('.invoice_paid').innerText = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(invoice.data.total_paid)
				this.target.querySelector('.invoice_total').innerText = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(invoice.data.total_tax_inclusive)
				let percent = Math.round(100 * invoice.data.total_paid / invoice.data.total_tax_inclusive)
				this.target.querySelector('.progress').value = percent
				this.target.querySelector('.invoice_paid_percent').innerText = percent
				this.target.querySelector('.progress').classList.toggle('is-primary', percent == 100)
				this.target.querySelector('.progress').classList.toggle('is-warning', (percent >= 50 && percent < 100))
				this.target.querySelector('.progress').classList.toggle('is-danger', percent < 50)
				this.target.querySelector('.add-button').classList.toggle('is-hidden', percent == 100)
			})
	}

	onUnload()
	{
		this.tabulator.destroy()
		delete this.tabulator
		return this
	}
}