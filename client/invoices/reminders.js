load_CSS('/libs/tabulator/css/tabulator_bulma.min.css')
load_script('/libs/luxon.min.js')
import { Tabulator, EditModule, FormatModule, InteractionModule, KeybindingsModule, MutatorModule, SortModule, AjaxModule } from '/libs/tabulator/js/tabulator_esm.min.js'
Tabulator.registerModule([EditModule, FormatModule, InteractionModule, KeybindingsModule, MutatorModule, SortModule, AjaxModule])
export default class OptimusBusinessEditorReminders
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		if (!store.queryParams.id)
			return this.target.innerHTML = "Aucun identifiant (id) n'a été renseigné"

		loader(this.target, true)
		await load('/services/optimus-business/invoices/reminders.html', this.target)

		let datepicker = await load('/components/optimus_table/datepicker.js')

		let invoices_reminders_types = await rest('https://' + this.server + '/optimus-business/constants/invoices_reminders_types').then(response => response.data.map(item => { return { label: item.value, value: item.id } }))

		this.target.querySelector('.add-button').onclick = () =>
			rest('https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id + '/reminders', 'POST')
				.then(response => response.code == 201 && this.tabulator.addRow(response.data))

		this.tabulator = new Tabulator(this.target.querySelector('.optimus-table-container'),
			{
				ajaxURL: 'https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id + '/reminders',
				ajaxRequestFunc: (url, ajaxConfig, params) => new Promise(function (resolve, reject)
				{
					if (!store.user.server)
						modal.open('/services/optimus-base/login/index.js')
					else
						rest(url, ajaxConfig.method, params, 'toast')
							.then(response =>
							{
								if (response.code >= 400)
									reject(response.message)
								else
									resolve(response.data)
							})
				}),
				ajaxConfig: {
					method: 'GET',
					credentials: 'include'
				},
				initialSort: [{ column: 'date', dir: 'desc' }],
				dataLoader: false,
				ajaxContentType: 'json',
				layout: 'fitColumns',
				layoutColumnsOnNewData: false,
				sortOrderReverse: true,
				headerSortElement: "<i class='fas fa-arrow-up'></i>",
				locale: 'fr',
				langs: {
					'fr': {
						ajax: {
							loading: "Chargement",
							error: "Erreur de chargement",
						},
					}
				},
				columns:
					[
						{
							field: 'date',
							title: 'Date',
							hozAlign: 'center',
							minWidth: 120,
							maxWidth: 120,
							formatter: 'datetime',
							formatterParams:
							{
								inputFormat: "yyyy-MM-dd",
								outputFormat: "dd/MM/yyyy",
								invalidPlaceholder: '<span class="has-text-danger">(date invalide)</span>',
							},
							editor: datepicker.open,
							editorParams:
							{
								optimusDatepicker:
								{
									type: 'date',
									showClearButton: true,
									closeOnOverlayClick: true,
									required: true
								}
							},
							cellEdited: cell => rest('https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id + '/reminders/' + cell.getData().id, 'PATCH', { date: cell.getValue() })
						},
						{
							field: 'type',
							title: 'Type',
							editor: 'list',
							editorParams:
							{
								values: invoices_reminders_types,
								itemFormatter: (label, value, item, element) => label,
								sort: (aLabel, bLabel, aValue, bValue, aItem, bItem) => aValue < bValue ? -1 : 1,
							},
							formatter: cell => invoices_reminders_types?.filter(item => item.value == cell.getValue())[0]?.label,
							mutator: value => value.toString(),
							cellEdited: cell => rest('https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id + '/reminders/' + cell.getData().id, 'PATCH', { type: cell.getValue() })
						},
						{
							field: 'delete',
							title: 'delete',
							hozAlign: 'center',
							minWidth: 44,
							maxWidth: 44,
							titleFormatter: () => '',
							formatter: () => '<i class="fas fa-trash"></i>',
							cellMouseOver: (event, cell) => 
							{
								cell.getElement().classList.add('is-clickable')
								cell.getElement().querySelector('i').classList.add('has-text-danger')
							},
							cellMouseOut: (event, cell) => cell.getElement().querySelector('i').classList.remove('has-text-danger'),
							cellClick: (event, cell) => rest('https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id + '/reminders/' + cell.getData().id, 'DELETE')
								.then(response => response.code == 200 && cell.getRow().delete())
						},
					]
			})

		this.tabulator.on("dataLoadError", () => loader(this.target, false))
		this.tabulator.on("dataProcessed", () => 
		{
			setTimeout(() => this.tabulator.element.classList.toggle('is-hidden', this.tabulator.getData().length == 0), 1)
			loader(this.target, false)
		})
		this.tabulator.on("dataChanged", data => this.tabulator.element.classList.toggle('is-hidden', data.length == 0))


	}

	onUnload()
	{
		this.tabulator.destroy()
		delete this.tabulator
		return this
	}
}