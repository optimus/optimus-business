
export default class OptimusBusinessInvoices
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		return this
	}

	async init()
	{
		if (!store.queryParams.owner)
		{
			let authorizations = await rest('https://' + store.user.server + '/optimus-base/authorizations', 'GET', { user: store.user.id, resource: 'invoices' })
			if (authorizations.data.length == 0)
				return optimusToast('Vous devez créer une entreprise et y être associé avant de pouvoir établir des factures', 'is-warning')
			return router('optimus-business/invoices?owner=' + authorizations.data[0].owner, false)
		}

		await load('/components/optimus_table/index.js', this.target,
			{
				id: 'invoices',
				version: 2,
				title: 'Factures',
				resource: 'invoices',
				showAddButton: false,
				url: 'https://' + store.user.server + '/optimus-business/owner/invoices',
				owner_type: 2,
				striped: true,
				bordered: true,
				columnSeparator: true,
				fullscreen: true,
				tabulator:
				{
					progressiveLoad: 'auto',
					columns:
						[
							{
								field: 'pdf',
								title: 'Pdf',
								formatter: cell => '<i class="fas fa-file-pdf"></i>',
								titleFormatter: cell => '<i class="fas fa-file-pdf"></i>',
								hozAlign: 'center',
								headerHozAlign: 'center',
								resizable: false,
								headerSort: false,
								print: false,
								download: false,
								clipboard: false,
								minWidth: 44,
								maxWidth: 44,
								cellMouseOver: (event, cell) => cell.getElement().querySelector('i')?.classList.add('has-text-link'),
								cellMouseOut: (event, cell) => cell.getElement().querySelector('i')?.classList.remove('has-text-link'),
								cellClick: (event, cell) => 
								{
									if (cell.getElement().parentNode.classList.contains('tabulator-calcs'))
										return false
									cell.getElement().querySelector('i').classList.replace('fa-file-pdf', 'fa-spinner')
									cell.getElement().querySelector('i').classList.add('fa-spin')
									rest('https://' + store.user.server + '/optimus-business/' + store.queryParams.owner + '/invoices/' + cell.getData().id + '/pdf')
										.then(file => 
										{
											if (file.code == 200)
											{
												var element = document.createElement('a')
												element.href = 'data:application/octet-stream;base64,' + file.data
												element.download = cell.getData().displayname + '.pdf'
												element.click()
												element.remove()
											}
											cell.getElement().querySelector('i').classList.replace('fa-spinner', 'fa-file-pdf')
											cell.getElement().querySelector('i').classList.remove('fa-spin')
										})
								}
							},
							{
								field: 'view_pdf',
								title: 'Pdf',
								formatter: cell => '<i class="fas fa-eye"></i>',
								titleFormatter: cell => '<i class="fas fa-eye"></i>',
								hozAlign: 'center',
								headerHozAlign: 'center',
								resizable: false,
								headerSort: false,
								print: false,
								download: false,
								clipboard: false,
								minWidth: 44,
								maxWidth: 44,
								cellMouseOver: (event, cell) => cell.getElement().querySelector('i')?.classList.add('has-text-link'),
								cellMouseOut: (event, cell) => cell.getElement().querySelector('i')?.classList.remove('has-text-link'),
								cellClick: (event, cell) => 
								{
									if (cell.getElement().parentNode.classList.contains('tabulator-calcs'))
										return false
									if (store.navigator == "Safari")
										return window.open('https://' + store.user.server + '/optimus-business/' + store.queryParams.owner + '/invoices/' + cell.getData().id + '/pdf?mode=download')
									cell.getElement().querySelector('i').classList.replace('fa-eye', 'fa-spinner')
									cell.getElement().querySelector('i').classList.add('fa-spin')
									rest('https://' + store.user.server + '/optimus-business/' + store.queryParams.owner + '/invoices/' + cell.getData().id + '/pdf')
										.then(file => 
										{
											if (file.code == 200)
											{
												let byteCharacters = atob(file.data)
												let byteNumbers = new Array(byteCharacters.length)
												for (let i = 0; i < byteCharacters.length; i++)
													byteNumbers[i] = byteCharacters.charCodeAt(i)
												let byteArray = new Uint8Array(byteNumbers)
												let pdfFile = new Blob([byteArray], { type: 'application/pdf;base64' })
												let fileURL = URL.createObjectURL(pdfFile)
												window.open(fileURL)
											}
											cell.getElement().querySelector('i').classList.replace('fa-spinner', 'fa-eye')
											cell.getElement().querySelector('i').classList.remove('fa-spin')
										})
								}
							},
							{
								field: 'displayname',
								title: 'Numéro',
								minWidth: 110,
								width: 110,
							},
							{
								field: 'client_displayname',
								title: 'Client',
								width: 200,
							},
							{
								field: 'owner_displayname',
								title: 'Créateur',
								width: 200,
							},
							{
								field: 'reference_displayname',
								title: 'Référence',
								width: 200,
							},
							{
								field: 'date',
								title: 'Date',
								minWidth: 120,
								width: 120,
								formatter: cell => cell.getValue() ? new Date(cell.getValue()).toLocaleDateString('fr') : '',
								hozAlign: 'center',
							},
							{
								field: 'collectible',
								title: 'Recouvrable',
								formatterClipboard: cell => 
								{
									if (cell.getValue() === true)
										return 1
									else if (cell.getValue() === false)
										return 0
									else
										return ''
								},
								formatter: cell => 
								{
									if (cell.getValue() === true)
										return '<i class="fas fa-check has-text-success"></i>'
									else if (cell.getValue() === false)
										return '<i class="fas fa-xmark has-text-danger"></i>'
									else
										return ''
								},
								hozAlign: 'center',
							},
							{
								field: 'total_tax_exclusive',
								title: 'Total HT',
								formatter: "money",
								formatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
								hozAlign: 'right',
								bottomCalc: 'sum',
								bottomCalcFormatter: 'money',
								bottomCalcFormatter: (cell, params, callback) =>
								{
									let money = cell.getTable().modules.format.constructor.formatters.money
									cell.getElement().classList.add('has-text-weight-normal', 'pr-4')
									return money(cell, params, callback)
								},
								bottomCalcFormatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
								minWidth: 120
							},
							{
								field: 'total_vat',
								title: 'Total TVA',
								visible: false,
								formatter: "money",
								formatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
								hozAlign: 'right',
								bottomCalc: 'sum',
								bottomCalcFormatter: 'money',
								bottomCalcFormatter: (cell, params, callback) =>
								{
									let money = cell.getTable().modules.format.constructor.formatters.money
									cell.getElement().classList.add('has-text-weight-normal', 'pr-4')
									return money(cell, params, callback)
								},
								bottomCalcFormatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
								minWidth: 120,
							},
							{
								field: 'total_tax_inclusive',
								title: 'Total TTC',
								formatter: "money",
								formatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
								hozAlign: 'right',
								bottomCalc: 'sum',
								bottomCalcFormatter: 'money',
								bottomCalcFormatter: (cell, params, callback) =>
								{
									let money = cell.getTable().modules.format.constructor.formatters.money
									cell.getElement().classList.add('has-text-weight-normal', 'pr-4')
									return money(cell, params, callback)
								},
								bottomCalcFormatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
								minWidth: 120,
							},
							{
								field: 'total_paid',
								title: 'Payé',
								formatter: 'money',
								formatter: (cell, params, callback) =>
								{
									let money = cell.getTable().modules.format.constructor.formatters.money
									cell.getElement().classList.add('has-text-success')
									return money(cell, params, callback)
								},
								formatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
								hozAlign: 'right',
								bottomCalc: 'sum',
								bottomCalcFormatter: 'money',
								bottomCalcFormatter: (cell, params, callback) =>
								{
									let money = cell.getTable().modules.format.constructor.formatters.money
									cell.getElement().classList.add('has-text-success', 'has-text-weight-normal', 'pr-4')
									return money(cell, params, callback)
								},
								bottomCalcFormatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
								minWidth: 120
							},
							{
								field: 'total_unpaid',
								title: 'Impayé',
								//formatter: 'money',
								formatter: (cell, params, callback) =>
								{
									let money = cell.getTable().modules.format.constructor.formatters.money
									cell.getElement().classList.add('has-text-danger')
									return money(cell, params, callback)
								},
								formatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
								hozAlign: 'right',
								bottomCalc: 'sum',
								bottomCalcFormatter: 'money',
								bottomCalcFormatter: (cell, params, callback) =>
								{
									let money = cell.getTable().modules.format.constructor.formatters.money
									cell.getElement().classList.add('has-text-danger', 'has-text-weight-normal', 'pr-4')
									return money(cell, params, callback)
								},
								bottomCalcFormatterParams: { decimal: ",", thousand: " ", symbol: " €", symbolAfter: true, precision: 2 },
								minWidth: 120
							},
							{
								field: 'percent_paid',
								title: '%',
								formatter: cell => 
								{
									if (cell.getValue() >= 100)
										return '<span class="has-text-success">' + cell.getValue() + ' %</span>'
									else if (cell.getValue() || cell.getValue() === 0)
										return '<span class="has-text-danger">' + cell.getValue() + ' %</span>'
									else
										return ''
								},
								hozAlign: 'right',
								minWidth: 70,
							},
						],
				},
				rowClick: (event, row) =>
				{
					if (event.target.getAttribute('tabulator-field') == 'pdf' || event.target.classList.contains('fa-file-pdf') || event.target.getAttribute('tabulator-field') == 'view_pdf' || event.target.classList.contains('fa-eye'))
						return false
					router('optimus-business/invoices/editor?owner=' + store.queryParams.owner + '&id=' + row.getData().id + '#general')
				}
			})
	}
}