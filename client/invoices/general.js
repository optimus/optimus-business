export default class OptimusBusinessEditorGeneral
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		if (admin_only()) return


		if (!store.queryParams.id)
			return this.target.innerHTML = "Aucun identifiant (id) n'a été renseigné"

		await load('/services/optimus-business/invoices/general.html', this.target, null, true)
		loader(this.target, true, 'list')
		const inputCollection = document.querySelectorAll("input,select,textarea")
		for (const el of inputCollection)
		{
			el.setAttribute('data-endpoint', '{server}/optimus-business/' + this.owner + '/invoices/{id}')
			el.setAttribute('data-field', el.id)
		}
		const invoice = await rest('https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id)

		await this.populate_templates(this.owner)

		await load('/components/optimus_form.js', this.target, invoice.data)
			.then(invoice => 
			{
				loader(this.target, false)
				main.querySelector('.total_tax_exclusive').innerText = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(invoice.data.total_tax_exclusive)
				main.querySelector('.total_vat').innerText = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(invoice.data.total_vat)
				main.querySelector('.disbursements').innerText = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(invoice.data.disbursements)
				main.querySelector('.total_tax_inclusive').innerText = new Intl.NumberFormat('fr-FR', { style: 'currency', currency: 'EUR' }).format(invoice.data.total_tax_inclusive)
				rest('https://api.' + invoice.data.client_server + '/optimus-contacts/' + invoice.data.client_owner + '/contacts/' + invoice.data.client_id)
					.then(client => 
					{
						if (client.code != 200)
							return
						main.querySelector('.client').value = client.data.displayname
						main.querySelector('.client').classList.add('is-clickable', 'has-text-link')
						main.querySelector('.client').onclick = () => router('optimus-contacts/contacts/editor?owner=' + invoice.data.client_owner + '&id=' + invoice.data.client_id + '#general')
					})
				rest('https://api.' + invoice.data.reference_server + invoice.data.reference_endpoint.replace('{owner}', invoice.data.reference_owner).replace('{id}', invoice.data.reference_id))
					.then(reference => 
					{
						if (reference.code != 200)
							return
						main.querySelector('.reference').value = reference.data.displayname
						main.querySelector('.reference').classList.add('is-clickable', 'has-text-link')
						main.querySelector('.reference').onclick = () => router('optimus-avocats/dossiers/editor?owner=' + invoice.data.reference_owner + '&id=' + invoice.data.reference_id + '#general')
					})
			})

		main.querySelector('.view-pdf-button').onclick = () => 
		{
			if (store.navigator == "Safari")
				return window.open('https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id + '/pdf?mode=view')
			main.querySelector('.view-pdf-button').classList.add('is-loading')
			rest('https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id + '/pdf')
				.then(file => 
				{
					if (file.code == 200)
					{
						let byteCharacters = atob(file.data)
						let byteNumbers = new Array(byteCharacters.length)
						for (let i = 0; i < byteCharacters.length; i++)
							byteNumbers[i] = byteCharacters.charCodeAt(i)
						let byteArray = new Uint8Array(byteNumbers)
						let pdfFile = new Blob([byteArray], { type: 'application/pdf;base64' })
						let fileURL = URL.createObjectURL(pdfFile)
						window.open(fileURL)
					}
					main.querySelector('.view-pdf-button').classList.remove('is-loading')
				})
		}

		main.querySelector('.download-pdf-button').onclick = () => 
		{
			main.querySelector('.download-pdf-button').classList.add('is-loading')
			rest('https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id + '/pdf')
				.then(file => 
				{
					if (file.code == 200)
					{
						var element = document.createElement('a')
						element.href = 'data:application/octet-stream;base64,' + file.data
						element.download = document.getElementById('displayname').value + '.pdf'
						element.click()
						element.remove()
					}
					main.querySelector('.download-pdf-button').classList.remove('is-loading')
				})
		}

		main.querySelector('.download-odt-button').onclick = () => 
		{
			main.querySelector('.download-odt-button').classList.add('is-loading')
			rest('https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id + '/odt')
				.then(file => 
				{
					if (file.code == 200)
					{
						var element = document.createElement('a')
						element.href = 'data:application/vnd.oasis.opendocument.text;base64,' + file.data
						element.download = document.getElementById('displayname').value + '.odt'
						element.click()
						element.remove()
					}
					main.querySelector('.download-odt-button').classList.remove('is-loading')
				})
		}
	}

	async populate_templates(business_id)
	{
		clear(main.querySelector('.template'))
		let business = await rest(store.user.server + '/optimus-base/users/' + business_id, 'GET')

		return await fetch('https://' + store.user.server.replace('api', 'cloud') + '/files/' + business.data.email + '/modeles/factures',
			{
				method: 'PROPFIND',
				credentials: 'include'
			})
			.then(response => response.text())
			.then(response => new DOMParser().parseFromString(response, 'text/xml'))
			.then(nodes => nodes.getElementsByTagName('d:response'))
			.then(nodes =>
			{
				let files = []
				for (let node of nodes)
					if (node.getElementsByTagName('d:collection').length == 0)
						files.push(
							{
								path: decodeURIComponent(node.getElementsByTagName('d:href')[0].childNodes[0].nodeValue.slice(6)),
								displayname: decodeURIComponent(node.getElementsByTagName('d:href')[0].childNodes[0].nodeValue.split('/').at(-1)),
								lastmodified: node.getElementsByTagName('d:getlastmodified')[0].childNodes[0].nodeValue,
							}
						)
				files.sort((a, b) => a.lastmodified < b.lastmodified ? 1 : -1)
				files.forEach(file => main.querySelector('.template').add(new Option(file.displayname, file.displayname)))
			})
	}
}