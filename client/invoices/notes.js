export default class OptimusAvocatsDossiers
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
	}

	async init()
	{
		if (!store.queryParams.id)
			return this.target.innerHTML = "Aucun identifiant (id) n'a été renseigné"

		await load('/services/optimus-business/invoices/notes.html', this.target)

		this.target.querySelector('.textarea').style.minHeight = (window.innerHeight - this.target.querySelector('.textarea').getBoundingClientRect().top - 32) + 'px'

		loader(this.target, true)
		let response = await rest('https://' + this.server + '/optimus-business/' + this.owner + '/invoices/' + this.id)
		await load('/components/optimus_form.js', main, response.data)
		loader(this.target, false)
	}
}