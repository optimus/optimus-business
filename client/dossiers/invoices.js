export default class OptimusAvocatsDossiersFactures
{
	constructor(target, params) 
	{
		this.target = target
		this.params = params
		this.server = store.queryParams.server || store.user.server
		this.owner = store.queryParams.owner || store.user.id
		this.id = store.queryParams.id
		return this
	}

	async init()
	{
		await load('/services/optimus-business/invoices_tab/index.js', this.target,
			{
				initial_filters:
					[
						{ field: 'reference_endpoint', type: '=', value: '/optimus-avocats/{owner}/dossiers/{id}' },
						{ field: 'reference_server', type: '=', value: store.user.server.replace('api.', '') },
						{ field: 'reference_owner', type: '=', value: store.queryParams.owner },
						{ field: 'reference_id', type: '=', value: store.queryParams.id },
					]
			})
	}
}