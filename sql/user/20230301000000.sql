CREATE TABLE IF NOT EXISTS `businesses` (
  `server` varchar(128) DEFAULT NULL,
  `business_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`server`,`business_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;