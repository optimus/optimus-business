DROP TABLE IF EXISTS partners;
DROP TABLE IF EXISTS incomes;
DROP TABLE IF EXISTS invoices;
DROP TABLE IF EXISTS invoices_details;
DROP TABLE IF EXISTS invoices_reminders;