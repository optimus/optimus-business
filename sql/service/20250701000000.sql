USE `server`;

ALTER TABLE IF EXISTS `businesses` ADD COLUMN `invoices_numbering_format` smallint(5) unsigned NOT NULL DEFAULT 1 AFTER `vat_scheme`;
ALTER TABLE IF EXISTS `businesses` ADD COLUMN `invoices_numbering_restart` bit(1) NOT NULL DEFAULT b'1' AFTER `invoices_numbering_format`;


USE `common`;

CREATE TABLE IF NOT EXISTS `invoices_numbering_formats` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

INSERT IGNORE INTO `invoices_numbering_formats` VALUES (1, "YY-#####");
INSERT IGNORE INTO `invoices_numbering_formats` VALUES (2, "######");