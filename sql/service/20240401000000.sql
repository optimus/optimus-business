USE `server`;

CREATE TABLE IF NOT EXISTS `businesses` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`name` varchar(255) DEFAULT NULL,
	`type` smallint(5) unsigned NOT NULL DEFAULT 0,
	`capital` varchar(255) DEFAULT NULL,
	`siren` varchar(16) DEFAULT NULL,
	`address` text DEFAULT NULL,
	`zipcode` varchar(16) DEFAULT NULL,
	`city` varchar(255) DEFAULT NULL,
	`city_name` varchar(255) DEFAULT NULL,
	`country` int(10) unsigned NOT NULL DEFAULT 74,
	`phone` varchar(255) DEFAULT NULL,
	`fax` varchar(255) DEFAULT NULL,
	`mobile` varchar(255) DEFAULT NULL,
	`email` varchar(255) DEFAULT NULL,
	`website` varchar(255) DEFAULT NULL,
	`iban` varchar(255) DEFAULT NULL,
	`bic` varchar(255) DEFAULT NULL,
	`vat_number` varchar(255) DEFAULT NULL,
	`accounting_method` smallint(5) unsigned NOT NULL DEFAULT 1,
	`vat_scheme` smallint(5) unsigned NOT NULL DEFAULT 1,
	`properties` longtext NOT NULL DEFAULT '{}',
	`displayname` varchar(128) GENERATED ALWAYS AS (coalesce(ucase(`name`),concat('ENTREPRISE N°', id))) VIRTUAL,
	PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;


USE `common`;

CREATE TABLE IF NOT EXISTS `accounting_methods` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

INSERT IGNORE INTO `accounting_methods` VALUES (10, "Trésorerie (recettes / dépenses)");
-- INSERT IGNORE INTO `accounting_methods` VALUES (20, "Engagement (créances / dettes)");


CREATE TABLE IF NOT EXISTS `invoices_reminders_types` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

INSERT IGNORE INTO `invoices_reminders_types` VALUES (10, "Appel téléphonique");
INSERT IGNORE INTO `invoices_reminders_types` VALUES (20, "Courriel");
INSERT IGNORE INTO `invoices_reminders_types` VALUES (30, "Courrier simple");
INSERT IGNORE INTO `invoices_reminders_types` VALUES (40, "Courrier recommandé");
INSERT IGNORE INTO `invoices_reminders_types` VALUES (50, "Sommation");
INSERT IGNORE INTO `invoices_reminders_types` VALUES (60, "Procédure judiciaire");


CREATE TABLE IF NOT EXISTS `vat_rates` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) DEFAULT NULL,
  `rate` decimal(4,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

INSERT IGNORE INTO `vat_rates` VALUES (10, "Ancien taux (19,6%)", 19.6);
INSERT IGNORE INTO `vat_rates` VALUES (20, "Taux Réduit (5,5%)", 5.5);
INSERT IGNORE INTO `vat_rates` VALUES (30, "Prestations Intra Communautaires", 0);
INSERT IGNORE INTO `vat_rates` VALUES (40, "Prestations Export", 0);
INSERT IGNORE INTO `vat_rates` VALUES (50, "Franchise en Base", 0);
INSERT IGNORE INTO `vat_rates` VALUES (60, "Taux Normal (20%)", 20);
INSERT IGNORE INTO `vat_rates` VALUES (70, "Taux Normal DOM (8,5%)", 8.5);


CREATE TABLE IF NOT EXISTS `vat_schemes` (
  `id` tinyint(4) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;

INSERT IGNORE INTO `vat_schemes` VALUES (1, "Réel normal");
INSERT IGNORE INTO `vat_schemes` VALUES (2, "Réel simplifié");
INSERT IGNORE INTO `vat_schemes` VALUES (3, "Franchise en base");