CREATE TABLE IF NOT EXISTS `partners` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`server` varchar(64) DEFAULT NULL,
	`user` int(10) unsigned DEFAULT NULL,
	`arrival` date DEFAULT NULL,
	`departure` date DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `invoices` (
	`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	`number` varchar(64) DEFAULT NULL,
	`date` date DEFAULT NULL,
	`total_tax_exclusive` decimal(10,2) NOT NULL DEFAULT 0.00,
	`total_tax_inclusive` decimal(10,2) NOT NULL DEFAULT 0.00,
	`total_vat` decimal(10,2) NOT NULL DEFAULT 0.00,
	`disbursements` decimal(10,2) NOT NULL DEFAULT 0.00,
	`vat_rate` tinyint(4) unsigned NOT NULL DEFAULT 6,
	`collectible` bit(1) NOT NULL DEFAULT b'1',
	`template` varchar(64) DEFAULT NULL,
	`client_endpoint` varchar(128) DEFAULT NULL,
	`client_server` varchar(64) DEFAULT NULL,
	`client_owner` int(10) unsigned DEFAULT NULL,
	`client_id` int(10) unsigned DEFAULT NULL,
	`client_displayname` varchar(255) DEFAULT NULL,
	`client_siren` varchar(16) DEFAULT NULL,
	`client_address` text DEFAULT NULL,
	`client_zipcode` varchar(16) DEFAULT NULL,
	`client_city` varchar(255) DEFAULT NULL,
	`client_city_name` varchar(255) DEFAULT NULL,
	`client_country` smallint(6) unsigned NOT NULL DEFAULT 74,
	`client_country_name` varchar(255) DEFAULT NULL,
	`details_endpoint` varchar(128) DEFAULT NULL,
	`details_server` varchar(64) DEFAULT NULL,
	`details_owner` int(10) unsigned DEFAULT NULL,
	`reference_endpoint` varchar(128) DEFAULT NULL,
	`reference_server` varchar(64) DEFAULT NULL,
	`reference_owner` int(10) unsigned DEFAULT NULL,
	`reference_id` int(10) unsigned DEFAULT NULL,
	`reference_displayname` varchar(255) DEFAULT NULL,
	`properties` longtext DEFAULT '{}',
	`notes` text DEFAULT NULL,
	`displayname` varchar(64) GENERATED ALWAYS AS (`number`) VIRTUAL,
	PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `invoices_details` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`date` date DEFAULT current_timestamp(),
	`description` text DEFAULT NULL,
	`category` tinyint(3) unsigned DEFAULT 1,
	`quantity` decimal(10,2) DEFAULT 0.00,
	`price` decimal(10,2) DEFAULT 0.00,
	`invoice_server` varchar(64) DEFAULT NULL,
	`invoice_owner` int(10) unsigned DEFAULT NULL,
	`invoice_id` int(10) unsigned DEFAULT NULL,
	`invoice_number` varchar(16) DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `invoices_reminders` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`invoice` int(10) unsigned DEFAULT NULL,
	`date` date DEFAULT NULL,
	`type` smallint(6) unsigned NOT NULL DEFAULT 0,
	PRIMARY KEY (`id`) USING BTREE,
	KEY `invoices_to_reminders` (`invoice`),
	CONSTRAINT `invoices_to_reminders` FOREIGN KEY (`invoice`) REFERENCES `invoices` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;
