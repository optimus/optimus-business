CREATE TABLE IF NOT EXISTS `invoices_payments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `transaction` int(11) DEFAULT NULL,
  `invoice` int(10) unsigned DEFAULT NULL,
  `date` date DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;